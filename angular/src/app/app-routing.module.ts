import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UsersComponent } from './users/users.component';
import { TenantsComponent } from './tenants/tenants.component';
import { RolesComponent } from 'app/roles/roles.component';
import { BeerStylesComponent } from './beerstyles/beerstyles.component';
import { FermentableIngredientsComponent } from '@app/fermentableIngredients/fermentableIngredients.component';
import { RecipeComponent } from '@app/recipe/recipe.component';
import { CreateRecipeComponent } from '@app/recipe/create-recipe/create-recipe.component';
import { EditRecipeComponent } from '@app/recipe/edit-recipe/edit-recipe.component';
import { BeerComponent } from '@app/beer/beer.component';
import { BeerMashingComponent } from '@app/beer/mashing/beer-mashing.component';
import { BeerBoilingComponent } from '@app/beer/boiling/beer-boiling.component';
import { BeerFermentationComponent } from '@app/beer/fermentation/beer-fermentation.component';
import { QuietFermentationComponent } from '@app/beer/quiet-fermentation/quiet-fermentation.component'
import { BottlingComponent } from '@app/beer/bottling/bottling.component'
import { WarehouseComponent } from '@app/warehouse/warehouse.component'

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AppComponent,
                children: [
                    { path: 'home', component: BeerComponent, data: { permission: 'Pages.Beers' }, canActivate: [AppRouteGuard]},
                    { path: 'users', component: UsersComponent, data: { permission: 'Pages.Users' }, canActivate: [AppRouteGuard] },
                    { path: 'roles', component: RolesComponent, data: { permission: 'Pages.Roles' }, canActivate: [AppRouteGuard] },
                    { path: 'tenants', component: TenantsComponent, data: { permission: 'Pages.Tenants' }, canActivate: [AppRouteGuard] },
                    { path: 'beerstyles', component: BeerStylesComponent, data: { permission: 'Pages.BeerStyles' }, canActivate: [AppRouteGuard] },
                    { path: 'ingredients', component: FermentableIngredientsComponent, data: { permission: 'Pages.Ingredients' }, canActivate: [AppRouteGuard] },
                    { path: 'warehouse', component: WarehouseComponent, data: { permission: 'Pages.Warehouse' }, canActivate: [AppRouteGuard] },
                    { path: 'recipes', component: RecipeComponent, data: { permission: 'Pages.BeerRecipes' }, canActivate: [AppRouteGuard] },
                    { path: 'recipes/new', component: CreateRecipeComponent, data: { permission: 'Pages.BeerRecipes' }, canActivate: [AppRouteGuard], canDeactivate: [AppRouteGuard] },
                    { path: 'recipes/edit/:id', component: EditRecipeComponent, data: { permission: 'Pages.BeerRecipes' }, canActivate: [AppRouteGuard], canDeactivate: [AppRouteGuard] },
                    { path: 'beers', component: BeerComponent, data: { permission: 'Pages.Beers' }, canActivate: [AppRouteGuard] },
                    { path: 'beers/1/:id', component: BeerMashingComponent, data: { permission: 'Pages.Beers' }, canActivate: [AppRouteGuard], canDeactivate: [AppRouteGuard] },
                    { path: 'beers/2/:id', component: BeerBoilingComponent, data: { permission: 'Pages.Beers' }, canActivate: [AppRouteGuard], canDeactivate: [AppRouteGuard]  },
                    { path: 'beers/3/:id', component: BeerFermentationComponent, data: { permission: 'Pages.Beers' }, canActivate: [AppRouteGuard], canDeactivate: [AppRouteGuard] },
                    { path: 'beers/4/:id', component: QuietFermentationComponent, data: { permission: 'Pages.Beers' }, canActivate: [AppRouteGuard], canDeactivate: [AppRouteGuard] },
                    { path: 'beers/5/:id', component: BottlingComponent, data: { permission: 'Pages.Beers' }, canActivate: [AppRouteGuard], canDeactivate: [AppRouteGuard] },
                    { path: 'about', component: AboutComponent }
                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
