import { Component, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { BeerRecipeServiceProxy, BeerRecipeDto, PagedResultDtoOfBeerRecipeDto, CreateBeerDto, BeerDto, BeerServiceProxy } from '@shared/service-proxies/service-proxies';
import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import { Router } from '@angular/router'
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './recipe.component.html',
    animations: [appModuleAnimation()],
})
export class RecipeComponent extends PagedListingComponentBase<BeerRecipeDto> {

    recipes: BeerRecipeDto[] = [];

    constructor(
        injector: Injector,
        private _router: Router,
        private _recipeService: BeerRecipeServiceProxy,
        private _beer: BeerServiceProxy
    ) {
        super(injector);
    }

    protected list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
        this._recipeService.getAll('', request.skipCount, request.maxResultCount)
            .pipe(finalize(() => {
                 finishedCallback()
            }))
            .subscribe((result: PagedResultDtoOfBeerRecipeDto) => {
                this.recipes = result.items;
                this.showPaging(result, pageNumber);
            });
    }

    protected delete(style: BeerRecipeDto): void {
      abp.message.confirm(
          "Delete recipe style '" + style.name + "'?",
          (result: boolean) => {
              if (result) {
                  this._recipeService.delete(style.id)
                      .subscribe(() => {
                          abp.notify.info("Deleted recipe: " + style.name);
                          this.refresh();
                      });
              }
          }
      );
    }

    createRecipe(): void {
      this._router.navigate(['app/recipes/new'])
    }

    editRecipe(id): void {
        this._router.navigate(['app/recipes/edit', id])
    }

    createBatch(batch: CreateBeerDto) {
        this._beer.createBeer(batch)
        .subscribe((result: number)=>{
            this._router.navigate(['app/beers/1', result])
        });
    }

    newBatch(recipe: BeerRecipeDto) {
        var batch = new CreateBeerDto();
        batch.name = recipe.name;
        batch.beerRecipeID = recipe.id;
        batch.actualStep = 1;
        this.createBatch(batch);
    }

    roundSRM(srm) {
        return Math.round(srm);
    }


    protected remove(recipe: BeerRecipeDto): void {
        abp.message.confirm(
            "Delete recipe '" + recipe.name + "'?",
            (result: boolean) => {
                if (result) {
                    this._recipeService.delete(recipe.id)
                        .subscribe(() => {
                            abp.notify.info("Deleted recipe: " + recipe.name);
                            this.refresh();
                        });
                }
            }
        );
    }
}
