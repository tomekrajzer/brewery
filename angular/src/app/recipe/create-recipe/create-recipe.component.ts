import { Component, Injector, ViewChild, AfterViewChecked, ElementRef, Output, EventEmitter, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { BeerRecipeServiceProxy, BeerStylesServiceProxy, FermentablesIngredientsServiceProxy,
     BeerRecipeDto, BeerStyleParams, FermentablesIngredientDto, FermentablesIngredient, FermentablesIngredientDtoIngredientType,
     IngredientUsedForRecipeDto, HopsUsedForRecipeServiceProxy, HopsUsedForRecipeDto, HopsServiceProxy, Hop, CreateHopsUsedForRecipeDto,
      HopDto, HopsUsedForRecipeDtoHoppingType, MashServiceProxy, MashDto, CreateBeerRecipeDto, BeerStylesDto, WarehouseServiceProxy, WarehouseIngredientDto, WarehouseHopDto, WarehouseDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { finalize } from 'rxjs/operators';
import { ConfigService } from './configuration.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { Router } from '@angular/router'
import { NgForm } from '@angular/forms';

@Component({
    selector: 'create-recipe',
    templateUrl: './create-recipe.component.html',
    animations: [appModuleAnimation()],
})

export class CreateRecipeComponent extends AppComponentBase implements OnInit, AfterViewChecked{

    @ViewChild('recipeContent') recipeContent: ElementRef;
    @ViewChild('createRecipeForm') breweryForm: NgForm;

    // @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    saving: boolean = false;
    styles: BeerStyleParams[] = [];
    recipe: BeerRecipeDto = new BeerRecipeDto();
    beerStyle: BeerStyleParams = new BeerStyleParams();
    allIngredients: FermentablesIngredientDto[] = [];
    ingredient: FermentablesIngredient;
    ingredientsUsedForRecipe: IngredientUsedForRecipeDto[] = [];
    hopsUsedForRecipe: HopsUsedForRecipeDto[] = [];
    warehouse: WarehouseDto;
    ingredientTypes: {id: number; name: string}[] = [];
    hoppingTypes: {id: number; name: string}[] = [];
    blgStyle: any = {min:null, max:null};
    hops: Hop[] = [];
    bitternessStadium: any;
    configuration;
    srmProgressType;
    wortSizeAfterBoiling;
    waterQuantityToMash;
    wortBLGBeforeBoiling;

    constructor(
        injector: Injector,
        private _recipeService: BeerRecipeServiceProxy,
        private _beerStyleService: BeerStylesServiceProxy,
        private _ingredientsService: FermentablesIngredientsServiceProxy,
        private _router: Router,
        private _warehouseService: WarehouseServiceProxy,
        private _hopsService: HopsServiceProxy,
    ) {
        super(injector);
        this.configuration = ConfigService.config;
    }

    ngOnInit(): void {
        this.initAllStylesList();
        this.getWarehouseItems();

        this.ingredientsUsedForRecipe = [];
        var tmpIngredientUsed = new IngredientUsedForRecipeDto();
        var tmpFermentableIngredient = new FermentablesIngredient();
        tmpIngredientUsed.ingredient = tmpFermentableIngredient;
        this.ingredientsUsedForRecipe.push(tmpIngredientUsed);

        this.hopsUsedForRecipe = [];
        var tmpHopsUsed = new HopsUsedForRecipeDto();
        var tmpHop = new HopDto();
        tmpHopsUsed.hop = tmpHop;
        this.hopsUsedForRecipe.push(tmpHopsUsed);

        this.recipe.boilTimeInMin = 60;
        this.recipe.evaporationRate = 10;
        this.recipe.lossesFromBoiling = 10;
        this.recipe.fermentationLosses = 5;
        this.recipe.coldHopingLosses = 0;

        for(var n in FermentablesIngredientDtoIngredientType) {
            if (typeof FermentablesIngredientDtoIngredientType[n] === 'number') {
                this.ingredientTypes.push({id: <any>FermentablesIngredientDtoIngredientType[n], name: n});
            }
        }

        for(var n in HopsUsedForRecipeDtoHoppingType) {
            if (typeof HopsUsedForRecipeDtoHoppingType[n] === 'number') {
                this.hoppingTypes.push({id: <any>HopsUsedForRecipeDtoHoppingType[n], name: n});
            }
        }
        this.recipe.mashEfficiency = 75;
        this.recipe.waterGrainRatio = 3;

        this.recipe.beerStyle = new BeerStyleParams();
    }

    ngAfterViewChecked() {
        $.AdminBSB.input.activate($(this.recipeContent.nativeElement));
      }

    initAllStylesList(): void {
        this._beerStyleService.getAllStyles()
            .subscribe((result: BeerStyleParams[])=>{
                this.styles = result;
            });
    }

    initIngredients(): void {
        this._ingredientsService.getAllIngredients()
            .subscribe((result: FermentablesIngredientDto[])=>{
                result.forEach(element => {
                    var qunatity = this.warehouse.ingredients.find(i => i.ingredientID === element.id);
                    if (qunatity)
                    {
                        element.name = element.name  + " --- <strong class=\"warehouse-info\">" + qunatity.quantity + " kg</strong>";
                    }
                    this.allIngredients.push(element);
                });
            });
    }

    initHops(): void {
        this._hopsService.getAllHops()
            .subscribe((result: Hop[])=>{
                result.forEach(element => {
                    var qunatity = this.warehouse.hops.find(i => i.hopID === element.id);
                    if (qunatity)
                    {
                        element.name = element.name  + " --- <em>" + qunatity.quantity + " g</em>";
                    }
                    this.hops.push(element);
                });
            });
    }

    save(): void {
        this.recipe.isPrivate = false;
        if (!this.ingredientsUsedForRecipe[this.ingredientsUsedForRecipe.length-1].ingredientID)
        {
            this.ingredientsUsedForRecipe = [];
        }
        if (!this.hopsUsedForRecipe[this.hopsUsedForRecipe.length-1].hopID)
        {
            this.hopsUsedForRecipe.splice(this.hopsUsedForRecipe.length-1,1);
        }
        var model = new CreateBeerRecipeDto();
        model.recipe = this.recipe;
        model.ingredients = this.ingredientsUsedForRecipe;
        model.hops = this.hopsUsedForRecipe;
        this.saving = true;
        this._recipeService.createRecipe(model)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.breweryForm.form.markAsPristine();
                this.notify.info(this.l('SavedSuccessfully'));
                this._router.navigate(['app/recipes'])
                // this.close();
                // this.modalSave.emit(null);
            });
    }

    recalculateBeerSize(): void {
        var beerSize = (this.recipe.beerSize +  (this.recipe.beerSize * (this.recipe.boilTimeInMin/60) * this.recipe.evaporationRate)/100);
        var boilingLosses = (beerSize * this.recipe.lossesFromBoiling) / 100;
        var fermentationLosses = (beerSize * this.recipe.fermentationLosses) / 100;
        var coldHopingLosses = (beerSize * this.recipe.coldHopingLosses) / 100;
        beerSize = beerSize + boilingLosses + fermentationLosses + coldHopingLosses;
        this.recipe.boilWortSize = +(beerSize.toFixed(1));
    }

    recalculateWortSize(): void {
        var fermentationLosses = (this.recipe.beerSize * this.recipe.fermentationLosses)/100;
        var coldHopingLosses = (this.recipe.beerSize * this.recipe.coldHopingLosses)/100;
        this.wortSizeAfterBoiling = this.recipe.beerSize + coldHopingLosses +  fermentationLosses;
        this.wortSizeAfterBoiling = +(this.wortSizeAfterBoiling.toFixed(1));
    }

    beerSizeChanged(size): void {
        this.recalculateBeerSize();
        this.recalculateWortSize();
        this.calculateSRM();
        this.calculateBLG();
        this.calculateABV();
        this.calculateIbu();
    }

    fermentationLossesChanged(size): void {
        this.recalculateBeerSize();
        this.recalculateWortSize();
    }

    coldHopingLossesChanged(size): void {
        this.recalculateBeerSize();
        this.recalculateWortSize();
    }

    onSelectStyle(event: TypeaheadMatch): void {
        this.recipe.beerStyleID = event.item.id;
        this.recipe.beerStyle = event.item;
        this.blgStyle.min = (1000*(this.recipe.beerStyle.oGmin-1)/4).toFixed(2);
        this.blgStyle.max = (1000*(this.recipe.beerStyle.oGmax-1)/4).toFixed(2);
    }

    addIngredient() {
        var tmpIngredientUsed = new IngredientUsedForRecipeDto();
        var tmpFermentableIngredient = new FermentablesIngredient();
        tmpIngredientUsed.ingredient = tmpFermentableIngredient;
        this.ingredientsUsedForRecipe.push(tmpIngredientUsed);
    }

    ingredientsUsedChanged(event, index){
        if (event.item.name.includes(" ---"))
        {
            event.item.name = event.item.name.substring(0, event.item.name.indexOf(' ---'));
        }
        this.ingredientsUsedForRecipe[index].ingredientID = event.item.id;
        this.ingredientsUsedForRecipe[index].ingredient = event.item;
        this.ingredientsUsedForRecipe[index].customEBC = event.item.ebc;
        this.ingredientsUsedForRecipe[index].customYield = event.item.yield;
        this.calculateSRM();
    }

    ingredientYieldChanged() {
        this.calculateBLG();
        this.calculateABV();
    }

    ingredientQuantityChanged(event, index) {
        var sumQuantity = 0;
        this.ingredientsUsedForRecipe[index].quantity = event;
        for(var item in this.ingredientsUsedForRecipe) {
            if (this.ingredientsUsedForRecipe[item].quantity)
            {
                sumQuantity += this.ingredientsUsedForRecipe[item].quantity;
            }
        }
        for(var item in this.ingredientsUsedForRecipe) {
            if (this.ingredientsUsedForRecipe[item].quantity)
            {
                this.ingredientsUsedForRecipe[item].percentageSize = Math.round((this.ingredientsUsedForRecipe[item].quantity/sumQuantity) * 100)
            }
        }
        if(sumQuantity < 1)
        {
            this.srmProgressType = "SRM1";
        }
        this.calculateSRM();
        this.calculateBLG();
        this.calculateABV();
        this.waterQuantityToMash = +(this.recipe.waterGrainRatio * sumQuantity).toFixed(2);
    }

    convertToLovibond(ebc) {
        return +(((ebc/1.97) + 0.76)/1.3546).toFixed(2);
    }

    calculateSRM()
    {
        var mcu  = 0.0;
        var ingredients = this.ingredientsUsedForRecipe;
        for(var item in ingredients) {
            var lovibond = 0.0;
            var lbs = 0.0;
            var gallons = 0.0;
            if (ingredients[item].customEBC) {
                lovibond = this.convertToLovibond(ingredients[item].customEBC);
            }
            else if (ingredients[item].ingredient.ebc) {
                lovibond = this.convertToLovibond(ingredients[item].ingredient.ebc);
            }
            if (ingredients[item].quantity) {
                lbs = ingredients[item].quantity * 2.2;
            }
            if (this.recipe.beerSize) {
                gallons = this.recipe.beerSize * 0.264;
            }
            if (lovibond && lbs && gallons)
            {
                mcu += (lovibond * lbs)/gallons;
            }
        }
        this.recipe.srm = +(Math.pow(mcu, 0.6859) * 1.4922).toFixed(2);
        this.srmProgressType = "SRM" + Math.round(this.recipe.srm);
        if (Math.round(this.recipe.srm) > 40)
        {
            this.srmProgressType = "SRM40";
        }
    }

    ingredientEBCChanged(event, index) {
        this.ingredientsUsedForRecipe[index].customEBC = event;
        this.calculateSRM();
    }

    lossesBoilingChanged(event) {

    }

    yieldToPPG(yieldValue: number)
    {
        return yieldValue/100 * 46.177 * 0.85; //http://www.howtobrew.com/book/section-2/what-is-malted-grain/table-of-typical-malt-yields
        //https://homebrew.stackexchange.com/questions/1434/wiki-how-do-you-calculate-original-gravity
    }

    calculateSG(beerSize)
    {
        var points = 0.0;
        var ingredients = this.ingredientsUsedForRecipe;
        var efficiency = this.recipe.mashEfficiency ? this.recipe.mashEfficiency : 75;
        for (var item in ingredients)
        {
            if (ingredients[item].customYield && ingredients[item].quantity){
                var yieldValue = (ingredients[item].customYield * efficiency)/100;
                var ppg = this.yieldToPPG(yieldValue);
                points += ((ingredients[item].quantity * 2.2) * ppg) / (beerSize * 0.264)
            }
        }
        return 1 + (points/1000);
    }

    calculateBLG() {
        var sg = this.calculateSG(this.recipe.beerSize);
        this.recipe.blg =  +(135.997 * Math.pow(sg, 3) - 630.272 * Math.pow(sg, 2) + 1111.14 * sg - 616.868).toFixed(1); //sg to blg
        this.calculateBoilBLG();
    }

    calculateBoilBLG() {
        var sg = this.calculateSG(this.recipe.boilWortSize);
        this.wortBLGBeforeBoiling =  +(135.997 * Math.pow(sg, 3) - 630.272 * Math.pow(sg, 2) + 1111.14 * sg - 616.868).toFixed(1); //sg to blg
    }

    calculateABV() {
        this.recipe.abv = +((this.recipe.blg - 2.56) / 1.938).toFixed(1); // Obliczanie_zawartości_alkoholu_w_gotowym_piwie blg koncowe = 2.56
    }

    remove(rowIndex: number): void {
        this.ingredientsUsedForRecipe.splice(rowIndex, 1);
        this.calculateSRM();
        this.calculateBLG();
        this.calculateABV();
        this.waterGrainRatioChanged(this.recipe.waterGrainRatio);
    }

    removeHop(rowIndex: number): void {
        this.hopsUsedForRecipe.splice(rowIndex, 1);
        this.calculateIbu();
    }

    addHop() {
        var tmpHopsUsed = new HopsUsedForRecipeDto();
        var tmpHop = new HopDto();
        tmpHopsUsed.hop = tmpHop;
        this.hopsUsedForRecipe.push(tmpHopsUsed);
    }

    hopSelected(event, index)
    {
        if (event.item.name.includes(" ---"))
        {
            event.item.name = event.item.name.substring(0, event.item.name.indexOf(' ---'));
        }
        this.hopsUsedForRecipe[index].customAlphaAcids = event.item.alphaAcids;
        this.hopsUsedForRecipe[index].hopID = event.item.id;
        this.hopsUsedForRecipe[index].hop = event.item;
    }

    calculateIbu(){

        var sumIBU = 0.0;
        var volume = this.recipe.beerSize;
        var gravity = this.calculateSG(this.recipe.beerSize);
        for(var item in this.hopsUsedForRecipe) {
            var hop = this.hopsUsedForRecipe[item];
            if (hop.hop && hop.hoppingType != 6)
            {
                if (hop.quantity > 0 && hop.boilingTimeInMinutes > 0 && hop.customAlphaAcids > 0)
                {
                    sumIBU += this.calculateUtilization(hop.boilingTimeInMinutes, gravity) * this.mglAA(hop.quantity, hop.customAlphaAcids/100, volume);
                }

            }
        }
        this.recipe.bitternessBalance = Math.round(sumIBU);

        if(this.recipe.bitternessBalance <= 11)
        {
            this.bitternessStadium = "MnimalBitterness";
        }
        else if (this.recipe.bitternessBalance <= 22)
        {
            this.bitternessStadium = "MediumBitterness";
        }
        else if (this.recipe.bitternessBalance <= 33)
        {
            this.bitternessStadium = "HighBitterness";
        }
        else
        {
            this.bitternessStadium = "MaxBitterness";
        }
    }

    calculateBigness(gravity){
        return 1.65*Math.pow(0.000125, (gravity-1));
    }

    calculateBtf(minutes){
        return (1-Math.pow(Math.E, (-0.04 * minutes)))/4.15
    }
    calculateUtilization(minutes, gravity){
        return this.calculateBtf(minutes)*this.calculateBigness(gravity);
    }

    mglAA(g, aa, volume){
        return (aa*g*1000)/volume;
    }

    mashEficiencyChanged() {
        this.calculateBLG();
        this.calculateABV();
    }

    waterGrainRatioChanged(ratio) {
        var sumQuantity = 0.0;
        for(var item in this.ingredientsUsedForRecipe) {
            if (this.ingredientsUsedForRecipe[item].quantity)
            {
                sumQuantity += this.ingredientsUsedForRecipe[item].quantity;
            }
        }
        this.waterQuantityToMash = +(sumQuantity * ratio).toFixed(2);
    }

    getIngredientTypeValue(ingredient: FermentablesIngredientDto) : string {
        if (ingredient.ingredientType)
        {
            return  this.ingredientTypes.find(i => i.id === ingredient.ingredientType).name
        }
        return undefined;
    }

    getWarehouseItems(): void {
        this._warehouseService.getWarehoseItems()
        .subscribe((result: WarehouseDto)=>{
            this.warehouse = result;

            if(this.warehouse.ingredients.length == 0)
            {
                this.warehouse.ingredients = [];
                var tmpIngredient = new WarehouseIngredientDto();
                var tmpFermentableIngredient = new FermentablesIngredient();
                tmpIngredient.ingredient = tmpFermentableIngredient;
                this.warehouse.ingredients.push(tmpIngredient);
            }
            if(this.warehouse.hops.length == 0)
            {
                this.warehouse.hops = [];
                var tmpWarehouseHop = new WarehouseHopDto();
                var tmpHop = new HopDto();
                tmpWarehouseHop.hop = tmpHop;
                this.warehouse.hops.push(tmpWarehouseHop);
            }
            this.initIngredients();
            this.initHops();
        });
    }

}
