import { Component, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FermentablesIngredientsServiceProxy, FermentablesIngredientDto, PagedResultDtoOfFermentablesIngredientDto } from '@shared/service-proxies/service-proxies';
import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import { CreateIngredientsComponent } from 'app/fermentableIngredients/create-ingredients/create-ingredients.component';
import { finalize } from 'rxjs/operators';
import { ConfigService } from '../ngx-configuration/configuration.service';


@Component({
    templateUrl: './fermentableIngredients.component.html',
    animations: [appModuleAnimation()]
})
export class FermentableIngredientsComponent extends PagedListingComponentBase<FermentablesIngredientDto> {

    @ViewChild('createIngredientModal') createIngredientModal: CreateIngredientsComponent;

    active: boolean = false;
    configuration;
    ingredients: FermentablesIngredientDto[] = [];
    columns = [
        { key: 'name', title: 'Name', orderEnabled: true },
        { key: 'yield', title: 'Yield' },
        { key: 'ebc', title: 'EBC' },
      ];
    constructor(
        injector: Injector,
        private _ingredientsService: FermentablesIngredientsServiceProxy
    ) {
        super(injector);
        this.configuration = ConfigService.config;
    }

    protected list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
        this._ingredientsService.getAll(request.skipCount, 10000)
            .pipe(finalize(() => {
                 finishedCallback()
            }))
            .subscribe((result: PagedResultDtoOfFermentablesIngredientDto) => {
                this.ingredients = result.items;
            });
    }

    protected delete(style: FermentablesIngredientDto): void {
        abp.message.confirm(
            "Sorry, this item couldn't be deleted",
        );
    }

    // Show Modals
    createIngredient(): void {
        this.createIngredientModal.show();
    }
}
