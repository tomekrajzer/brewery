import { Component, Injector, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FermentablesIngredientsServiceProxy, FermentablesIngredientDto, FermentablesIngredientDtoIngredientType } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'create-ingredient-modal',
    templateUrl: './create-ingredients.component.html',
    animations: [appModuleAnimation()]
})
export class CreateIngredientsComponent extends AppComponentBase {

    @ViewChild('createIngredientModal') modal: ModalDirective;
    @ViewChild('modalContent') modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    types: any = FermentablesIngredientDtoIngredientType;
    typeKeys: any = Object.keys(FermentablesIngredientDtoIngredientType).filter(Number);
    ingredient: FermentablesIngredientDto = null;

    constructor(
        injector: Injector,
        private _ingredientsService: FermentablesIngredientsServiceProxy
    ) {
        super(injector);
    }

    show(): void {
        this.active = true;
        this.modal.show();
        this.ingredient = new FermentablesIngredientDto();
        this.ingredient.init({ isActive: true });
    }

    onShown(): void {
        $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        this.saving = true;
        this._ingredientsService.create(this.ingredient)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
