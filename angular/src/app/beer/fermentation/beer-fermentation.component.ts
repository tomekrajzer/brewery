import { Component, Injector, ViewChild, OnInit, AfterViewChecked, ElementRef } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { BeerServiceProxy, BeerDto, CreateBeerDto, PagedResultDtoOfBeerDto,
     MashStepsServiceProxy, MashStepDto, FermentationServiceProxy, FermentationDto,
      HopsServiceProxy, HopDto, HopsUsedForRecipeDto, HopsUsedForRecipeServiceProxy, IngredientUsedForRecipeDto, IngredientsUsedForRecipeServiceProxy} from '@shared/service-proxies/service-proxies';
import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { Router, ActivatedRoute } from '@angular/router';
import { DatepickerOptions } from 'ng2-datepicker';
import * as moment from 'moment';
import * as enLocale from 'date-fns/locale/en';
import { threadId } from 'worker_threads';
import { NgForm } from '@angular/forms';

@Component({
    templateUrl: './beer-fermentation.component.html',
    animations: [appModuleAnimation()],
})
export class BeerFermentationComponent extends AppComponentBase implements OnInit, AfterViewChecked {
    @ViewChild('beerFermentationContent') beerFermentationContent: ElementRef;
    @ViewChild('breweryForm') breweryForm: NgForm;

    private sub: any;
    beer: BeerDto;
    mashSteps: MashStepDto[] = [];
    fermentationProcess: FermentationDto = new FermentationDto();
    beerID;
    hopsUsedForRecipe: HopsUsedForRecipeDto[] = [];
    ingredientsUsedForRecipe: IngredientUsedForRecipeDto[] = [];
    active: boolean = false;
    brewhouseEff;
    calendarOptions: DatepickerOptions = {
        displayFormat:"YYYY-MM-DD",
        locale: enLocale,
        useEmptyBarTitle: false,
        firstCalendarDay: 1,
        addClass: 'validate form-control'
    };

    constructor(
        injector: Injector,
        private _router: Router,
        private _beerService: BeerServiceProxy,
        private _activRoute: ActivatedRoute,
        private _mashStep: MashStepsServiceProxy,
        private _fermentation: FermentationServiceProxy,
        private _hopsService: HopsServiceProxy,
        private _hopsUsed: HopsUsedForRecipeServiceProxy,
        private _ingredientUsed: IngredientsUsedForRecipeServiceProxy

    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.sub = this._activRoute.params.subscribe(params => {
            this.beerID = +params['id']; // (+) converts string 'id' to a number
        });
        this.getBeer();
        this.getFermentationDetails();
    }

    ngAfterViewChecked() {
        $.AdminBSB.input.activate($(this.beerFermentationContent.nativeElement));
    }

    getBeer(): void {
        this._beerService.get(this.beerID)
            .subscribe((result: BeerDto) => {
                this.beer = result;
                this.getRecipeIngredients(this.beer.beerRecipeID);
            });
    }

    getRecipeIngredients(id) {
        this._ingredientUsed.getRecipeIngredients(id)
        .subscribe((result: IngredientUsedForRecipeDto[])=>{
            this.ingredientsUsedForRecipe = result;
            this.calculateBrewhouseEfficiency(this.beer.beerRecipe.beerSize);
            this.active = true;
        });
    }

    getFermentationDetails() {
        this._fermentation.getByBeer(this.beerID)
        .subscribe((result: FermentationDto) => {
            this.fermentationProcess = result;
        });
    }

    save(isNextStep): void {
        this.fermentationProcess.beerID = this.beerID;
        this._fermentation.insertOrUpdate(this.fermentationProcess)
            .subscribe(() => { });
        this._beerService.update(this.beer)
            .subscribe(() => {
                this.breweryForm.form.markAsPristine();
                this.notify.info(this.l('SavedSuccessfully'));
                if (isNextStep)
                {
                    this._router.navigate(['app/beers/4', this.beerID])
                }
            });
    }

    blgChanged() {
        this.beer.blg = this.fermentationProcess.blg;
        this.calculateBrewhouseEfficiency(this.beer.beerRecipe.beerSize);
    }

    nextStep(): void {
        this.beer.actualStep = 4;
        this.save(true);
    }

    goToProcess(process): void {
        if (this.beer.actualStep.valueOf() >= process)
        {
            this._router.navigate(['app/beers/' + process, this.beerID])
        }
    }

    yieldToPPG(yieldValue: number)
    {
        return yieldValue/100 * 46.177 * 0.85; //http://www.howtobrew.com/book/section-2/what-is-malted-grain/table-of-typical-malt-yields
        //https://homebrew.stackexchange.com/questions/1434/wiki-how-do-you-calculate-original-gravity
    }

    calculateBrewhouseEfficiency(beerSize)
    {
        var ingredientPoints = 0.0;
        var totalPoints = 0.0;
        var ingredients = this.ingredientsUsedForRecipe;
        for (var item in ingredients)
        {
            var yieldValue = (ingredients[item].customYield)/100;
            ingredientPoints = (40 * yieldValue)
            totalPoints += (ingredientPoints * (ingredients[item].quantity/0.45359237))/(this.fermentationProcess.wortSize * 0.264172)
        }
        var currentPoints = (this.fermentationProcess.blg / (258.6 - ( (this.fermentationProcess.blg/258.2) *227.1) ) )

        this.brewhouseEff = ((currentPoints*1000)/totalPoints * 100).toFixed(2);

    }
}
