import { Component, Injector, ViewChild, OnInit, AfterViewChecked, ElementRef } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { BeerServiceProxy, BeerDto, CreateBeerDto, PagedResultDtoOfBeerDto, MashStepsServiceProxy,
     MashStepDto, BeerRecipeServiceProxy, HopsServiceProxy, HopDto, HopsUsedForRecipeDto, WarehouseHopDto, WarehouseHopsServiceProxy,
      HopsUsedForRecipeServiceProxy, BoilServiceProxy, BoilDto} from '@shared/service-proxies/service-proxies';
import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { Router, ActivatedRoute } from '@angular/router';
import { DatepickerOptions } from 'ng2-datepicker';
import * as moment from 'moment';
import * as enLocale from 'date-fns/locale/en';
import { threadId } from 'worker_threads';
import { NgForm } from '@angular/forms';

@Component({
    templateUrl: './beer-boiling.component.html',
    animations: [appModuleAnimation()],
})
export class BeerBoilingComponent extends AppComponentBase implements OnInit, AfterViewChecked {
    @ViewChild('beerBoilContent') beerBoilContent: ElementRef;
    @ViewChild('breweryForm') breweryForm: NgForm;

    private sub: any;
    beer: BeerDto;
    mashSteps: MashStepDto[] = [];
    boilProcess: BoilDto = new BoilDto();
    beerID;
    warehouseHops;
    hopsUsedForRecipe: HopsUsedForRecipeDto[] = [];
    active: boolean = false;
    calendarOptions: DatepickerOptions = {
        displayFormat:"YYYY-MM-DD",
        locale: enLocale,
        useEmptyBarTitle: false,
        firstCalendarDay: 1,
        addClass: 'validate form-control'
    };

    constructor(
        injector: Injector,
        private _router: Router,
        private _beerService: BeerServiceProxy,
        private _activRoute: ActivatedRoute,
        private _mashStep: MashStepsServiceProxy,
        private _hopsService: HopsServiceProxy,
        private _hopsUsed: HopsUsedForRecipeServiceProxy,
        private _boil: BoilServiceProxy,
        private _warehouseHops: WarehouseHopsServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.sub = this._activRoute.params.subscribe(params => {
            this.beerID = +params['id']; // (+) converts string 'id' to a number
        });
        this.getBeer();
        this.getBoilDetails();
        this.getWarehouseHops();
    }

    ngAfterViewChecked() {
        $.AdminBSB.input.activate($(this.beerBoilContent.nativeElement));
    }

    getBeer(): void {
        this._beerService.get(this.beerID)
            .subscribe((result: BeerDto) => {
                this.beer = result;
                this.getRecipeHops();
                this.active = true;
            });
    }

    getBoilDetails() {
        this._boil.getByBeer(this.beerID)
        .subscribe((result: BoilDto) => {
            this.boilProcess = result;
        });
    }

    getRecipeHops() {
        this._hopsUsed.getHopsUsedForBoil(this.beer.beerRecipe.id)
        .subscribe((result: HopsUsedForRecipeDto[])=>{
            this.hopsUsedForRecipe = result;
        });
    }

    save(isNextStep): void {
        this.boilProcess.beerID = this.beerID;
        this._boil.insertOrUpdate(this.boilProcess)
            .subscribe(() => { });
        this._beerService.update(this.beer)
        .subscribe(() => {
            this.breweryForm.form.markAsPristine();
            this.notify.info(this.l('SavedSuccessfully'));
            if (isNextStep)
            {
                this._router.navigate(['app/beers/3', this.beerID])
            }
        });
    }

    nextStep(): void {
        this.beer.actualStep = 3;
        this.updateHopQunatity();
        this.save(true);
    }

    goToProcess(process): void {
        if (this.beer.actualStep.valueOf() >= process)
        {
            this._router.navigate(['app/beers/' + process, this.beerID])
        }
    }

    getWarehouseHops()
    {
        this._warehouseHops.getAllUserHops()
            .subscribe((result: WarehouseHopDto[]) => {
                this.warehouseHops = result;
            });
    }

    updateHopQunatity()
    {
        var hopToUpdate: WarehouseHopDto[] = [];
        this.hopsUsedForRecipe.forEach(element => {
            var hop = this.warehouseHops.find(x => x.hopID == element.hopID);
            if (!hop)
            {
                return;
            }
            hop.quantity -= element.quantity;
            if (hop.quantity <= 0)
            {
                this._warehouseHops.delete(hop.id)
                    .subscribe(() => { });
            }
            else if (!hopToUpdate.find(x => x.hopID == element.hopID))
            {
                hopToUpdate.push(hop);
            }
        });
        this._warehouseHops.updateQuantity(hopToUpdate)
            .subscribe(() => { });
    }
}


