import { Component, Injector, ViewChild, OnInit, AfterViewChecked, ElementRef, Input} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { BeerServiceProxy, BeerDto, CreateBeerDto, PagedResultDtoOfBeerDto, MashStepsServiceProxy,
     MashStepDto, BeerRecipeServiceProxy, MashDto, MashServiceProxy, CreateBeerDtoActualStep,
      IngredientsUsedForRecipeServiceProxy, IngredientUsedForRecipeDto, WarehouseServiceProxy, WarehouseIngredientsServiceProxy,
       WarehouseIngredientDto} from '@shared/service-proxies/service-proxies';
import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { Router, ActivatedRoute } from '@angular/router';
import { DatepickerOptions } from 'ng2-datepicker';
import * as moment from 'moment';
import * as enLocale from 'date-fns/locale/en';
import { threadId } from 'worker_threads';
import { NgForm } from '@angular/forms';

@Component({
    templateUrl: './beer-mashing.component.html',
    animations: [appModuleAnimation()],
})
export class BeerMashingComponent extends AppComponentBase implements OnInit, AfterViewChecked {
    @ViewChild('beerMashContent') beerMashContent: ElementRef;
    @ViewChild('mashingBeerForm') breweryForm: NgForm;

    private sub: any;
    beer: BeerDto;
    mashSteps: MashStepDto[] = [];
    mashProcess: MashDto = new MashDto();
    ingredientsUsedForRecipe: IngredientUsedForRecipeDto[] = [];
    beerID;
    waterMashQuantity;
    warehouseIngredients;
    active: boolean = false;
    calendarOptions: DatepickerOptions = {
        displayFormat:"YYYY-MM-DD",
        locale: enLocale,
        useEmptyBarTitle: false,
        firstCalendarDay: 1,
        addClass: 'validate form-control'
    };

    constructor(
        injector: Injector,
        private _router: Router,
        private _beerService: BeerServiceProxy,
        private _activRoute: ActivatedRoute,
        private _mashStep: MashStepsServiceProxy,
        private _mash: MashServiceProxy,
        private _warehouseService: WarehouseServiceProxy,
        private _ingredientUsed: IngredientsUsedForRecipeServiceProxy,
        private _warehouseIngredients: WarehouseIngredientsServiceProxy,
        private _werhouseIngredientService: WarehouseIngredientsServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.sub = this._activRoute.params.subscribe(params => {
            this.beerID = +params['id']; // (+) converts string 'id' to a number
        });
        this.getSteps();
        this.getBeer();
        this.getWarehouseIngredients();
    }

    ngAfterViewChecked() {
        $.AdminBSB.input.activate($(this.beerMashContent.nativeElement));
    }

    getBeer(): void {
        this._beerService.get(this.beerID)
            .subscribe((result: BeerDto) => {
                this.beer = result;
                this.getRecipeIngredients(this.beer.beerRecipeID);
                this.getMashDetails();
                if (!this.beer.beerNumber)
                {
                    this.getBeerNumber();
                }
                this.active = true;
            });
    }

    getBeerNumber(): void {
        this._beerService.getBeerNumber()
            .subscribe((result: number) => {
                this.beer.beerNumber = result;
            });
    }

    getRecipeIngredients(id) {
        this._ingredientUsed.getRecipeIngredients(id)
        .subscribe((result: IngredientUsedForRecipeDto[])=>{
            this.ingredientsUsedForRecipe = result;
        });
        this._ingredientUsed.calculateWaterToMash(this.beer.beerRecipeID, this.beer.beerRecipe.waterGrainRatio)
        .subscribe((result: number) => {
            this.waterMashQuantity = result.toFixed(2);
        });
    }

    getMashDetails(): void {
        this._mash.getByBeer(this.beerID)
            .subscribe((result: MashDto) => {
                this.mashProcess = result;
            });
    }

    getSteps() {
        this._mashStep.getByBeer(this.beerID)
            .subscribe((result: MashStepDto[]) => {
                this.mashSteps = result;
            });
    }

    addMashStep(): void {
        var mash = new MashStepDto();
        mash.beerID = this.beerID;
        this.mashSteps.push(mash);
    }

    mashTempChanged(index) {
        this.mashSteps[index].stepOrder = index;
    }

    removeMashStep(mashID)
    {
        this._mashStep.delete(mashID)
            .subscribe(() => { });
    }

    remove(rowIndex: number): void {
        this.removeMashStep(this.mashSteps[rowIndex].id);
        this.mashSteps.splice(rowIndex, 1);
    }

    save(isNextStep): void {
        this.mashProcess.beerID = this.beerID;
        this._mash.insertOrUpdate(this.mashProcess)
            .subscribe(() => { });
        this._mashStep.insertOrUpdate(this.mashSteps)
            .subscribe(() => { });
        this._beerService.update(this.beer)
        .subscribe(() => {
            this.breweryForm.form.markAsPristine();
            this.notify.info(this.l('SavedSuccessfully'));
            if (isNextStep)
            {
                this._router.navigate(['app/beers/2', this.beerID])
            }
        });
    }

    nextStep(): void {
        this.beer.actualStep = 2;
        this.updateIngredientQunatity(this.ingredientsUsedForRecipe);
        this.save(true);
    }

    goToProcess(process): void {
        if (this.beer.actualStep.valueOf() >= process)
        {
            this._router.navigate(['app/beers/' + process, this.beerID])
        }
    }

    getWarehouseIngredients()
    {
        this._warehouseIngredients.getAllUserIngredient()
            .subscribe((result: WarehouseIngredientDto[]) => {
                this.warehouseIngredients = result;
            });
    }

    updateIngredientQunatity(mashIngredients: IngredientUsedForRecipeDto[])
    {
        var ingredientToUpdate: WarehouseIngredientDto[] = [];
        mashIngredients.forEach(element => {
            var ingredient = this.warehouseIngredients.find(x => x.ingredientID == element.ingredientID);
            if (!ingredient)
            {
                return;
            }
            ingredient.quantity -= element.quantity;
            if (ingredient.quantity <= 0)
            {
                this._werhouseIngredientService.delete(ingredient.id)
                    .subscribe(() => { });
            }
            else
            {
                ingredientToUpdate.push(ingredient);
            }
        });
        this._werhouseIngredientService.updateQuantity(ingredientToUpdate)
            .subscribe(() => { });
    }
}
