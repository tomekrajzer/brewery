import { Component, Injector, ViewChild, OnInit, AfterViewChecked, ElementRef } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { BeerServiceProxy, BeerDto, CreateBeerDto, PagedResultDtoOfBeerDto,
     MashStepsServiceProxy, MashStepDto, BeerRecipeServiceProxy, QuietFermentationDto, QuietFermentationServiceProxy, WarehouseHopsServiceProxy, WarehouseHopDto,
      HopsServiceProxy, HopDto, HopsUsedForRecipeDto, HopsUsedForRecipeServiceProxy} from '@shared/service-proxies/service-proxies';
import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { Router, ActivatedRoute } from '@angular/router';
import { DatepickerOptions } from 'ng2-datepicker';
import * as moment from 'moment';
import * as enLocale from 'date-fns/locale/en';
import { threadId } from 'worker_threads';
import { NgForm } from '@angular/forms';
@Component({
    templateUrl: './quiet-fermentation.component.html',
    animations: [appModuleAnimation()],
})
export class QuietFermentationComponent extends AppComponentBase implements OnInit, AfterViewChecked {
    @ViewChild('quietFermentationContent') quietFermentationContent: ElementRef;
    @ViewChild('quietBeerForm') breweryForm: NgForm;

    private sub: any;
    beer: BeerDto;
    mashSteps: MashStepDto[] = [];
    qFermentation: QuietFermentationDto = new QuietFermentationDto();
    beerID;
    warehouseHops;
    hopsUsedForRecipe: HopsUsedForRecipeDto[] = [];
    active: boolean = false;
    calendarOptions: DatepickerOptions = {
        displayFormat:"YYYY-MM-DD",
        locale: enLocale,
        useEmptyBarTitle: false,
        firstCalendarDay: 1,
        addClass: 'validate form-control'
    };

    constructor(
        injector: Injector,
        private _router: Router,
        private _beerService: BeerServiceProxy,
        private _activRoute: ActivatedRoute,
        private _qFermentation: QuietFermentationServiceProxy,
        private _hopsService: HopsServiceProxy,
        private _hopsUsed: HopsUsedForRecipeServiceProxy,
        private _warehouseHops: WarehouseHopsServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.sub = this._activRoute.params.subscribe(params => {
            this.beerID = +params['id']; // (+) converts string 'id' to a number
        });
        this.getBeer();
        this.getFermentationDetails();
        this.getWarehouseHops();
    }

    ngAfterViewChecked() {
        $.AdminBSB.input.activate($(this.quietFermentationContent.nativeElement));
    }

    getBeer(): void {
        this._beerService.get(this.beerID)
            .subscribe((result: BeerDto) => {
                this.beer = result;
                this.getRecipeHops();
            });
    }

    getFermentationDetails() {
        this._qFermentation.getByBeer(this.beerID)
        .subscribe((result: QuietFermentationDto) => {
            this.qFermentation = result;
        });
    }

    getRecipeHops() {
        this._hopsUsed.getHopsUsedForFermentation(this.beer.beerRecipe.id)
        .subscribe((result: HopsUsedForRecipeDto[])=>{
            this.hopsUsedForRecipe = result;
            this.active = true;
        });
    }

    save(isNextStep): void {
        this.qFermentation.beerID = this.beerID;
        this._qFermentation.insertOrUpdate(this.qFermentation)
            .subscribe(() => { });
        this._beerService.update(this.beer)
            .subscribe(() => {
                this.breweryForm.form.markAsPristine();
                this.notify.info(this.l('SavedSuccessfully'));
                if (isNextStep)
                {
                    this._router.navigate(['app/beers/5', this.beerID])
                }
            });
    }

    nextStep(): void {
        this.beer.actualStep = 5;
        this.updateHopQunatity();
        this.save(true);
    }

    goToProcess(process): void {
        if (this.beer.actualStep.valueOf() >= process)
        {
            this._router.navigate(['app/beers/' + process, this.beerID])
        }
    }

    getWarehouseHops()
    {
        this._warehouseHops.getAllUserHops()
            .subscribe((result: WarehouseHopDto[]) => {
                this.warehouseHops = result;
            });
    }

    updateHopQunatity()
    {
        var hopToUpdate: WarehouseHopDto[] = [];
        this.hopsUsedForRecipe.forEach(element => {
            var hop = this.warehouseHops.find(x => x.hopID == element.hopID);
            if (!hop)
            {
                return;
            }
            hop.quantity -= element.quantity;
            if (hop.quantity <= 0)
            {
                this._warehouseHops.delete(hop.id)
                    .subscribe(() => { });
            }
            else
            {
                hopToUpdate.push(hop);
            }
        });
        this._warehouseHops.updateQuantity(hopToUpdate)
            .subscribe(() => { });
    }
}
