import { Component, Injector, ViewChild, OnInit, AfterViewChecked, ElementRef } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { BeerServiceProxy, BeerDto, CreateBeerDto, PagedResultDtoOfBeerDto, BottlingServiceProxy,
     BottlingDto, MashStepsServiceProxy, MashStepDto, BeerRecipeServiceProxy,
    HopsServiceProxy, HopDto, HopsUsedForRecipeDto, HopsUsedForRecipeServiceProxy,
    FermentationServiceProxy, FermentationDto} from '@shared/service-proxies/service-proxies';
import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { Router, ActivatedRoute } from '@angular/router';
import { DatepickerOptions } from 'ng2-datepicker';
import * as moment from 'moment';
import * as enLocale from 'date-fns/locale/en';
import { threadId } from 'worker_threads';
import { NgForm } from '@angular/forms';

@Component({
    templateUrl: './bottling.component.html',
    animations: [appModuleAnimation()],
})
export class BottlingComponent extends AppComponentBase implements OnInit, AfterViewChecked {
    @ViewChild('bottlingContent') bottlingContent: ElementRef;
    @ViewChild('bottlingForm') breweryForm: NgForm;

    private sub: any;
    beer: BeerDto;
    mashSteps: MashStepDto[] = [];
    bottlingProcess: BottlingDto = new BottlingDto();
    fermentationProcess: FermentationDto = new FermentationDto();
    endABV;
    beerID;
    calories;
    fermentationDg;
    sugarQuantity;
    glucoseQuantity;
    hopsUsedForRecipe: HopsUsedForRecipeDto[] = [];
    active: boolean = false;
    calendarOptions: DatepickerOptions = {
        displayFormat:"YYYY-MM-DD",
        locale: enLocale,
        useEmptyBarTitle: false,
        firstCalendarDay: 1,
        addClass: 'validate form-control'
    };

    constructor(
        injector: Injector,
        private _router: Router,
        private _beerService: BeerServiceProxy,
        private _activRoute: ActivatedRoute,
        private _mashStep: MashStepsServiceProxy,
        private _hopsService: HopsServiceProxy,
        private _hopsUsed: HopsUsedForRecipeServiceProxy,
        private _fermentation: FermentationServiceProxy,
        private _botlling: BottlingServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.sub = this._activRoute.params.subscribe(params => {
            this.beerID = +params['id']; // (+) converts string 'id' to a number
        });
        this.getBeer();
        this.getBottlingDetails();
        this.getFermentationDetails();
    }

    ngAfterViewChecked() {
        $.AdminBSB.input.activate($(this.bottlingContent.nativeElement));
        this.blgChanged();
    }

    getBottlingDetails() {
        this._botlling.getByBeer(this.beerID)
        .subscribe((result: BottlingDto) => {
            this.bottlingProcess = result;
            this.calculateSugar();
        });
    }

    getBeer(): void {
        this._beerService.get(this.beerID)
            .subscribe((result: BeerDto) => {
                this.beer = result;
                this.active = true;
            });
    }

    getFermentationDetails() {
        this._fermentation.getByBeer(this.beerID)
        .subscribe((result: FermentationDto) => {
            this.fermentationProcess = result;
        });
    }

    calculateABV() {
        this.endABV = +((this.fermentationProcess.blg - this.bottlingProcess.blg)/1.938).toFixed(1);
        this.calculteCalories();
    }

    calculateFermentationDg() {
        this.fermentationDg = +(((this.fermentationProcess.blg - this.bottlingProcess.blg)/this.fermentationProcess.blg)*100).toFixed(1);
    }

    blgChanged() {
        this.calculateABV();
        this.calculateFermentationDg();
    }

    calculteCalories() {
        this.calories = Math.round((this.endABV * 2.5 ) * 16.9)
    }

    save(isNextStep): void {
        this.bottlingProcess.beerID = this.beerID;
        this._botlling.insertOrUpdate(this.bottlingProcess)
            .subscribe(() => { });
        this._beerService.update(this.beer)
            .subscribe(() => {
                this.breweryForm.form.markAsPristine();
                this.notify.info(this.l('SavedSuccessfully'));
                if (isNextStep)
                {
                    this._router.navigate(['app/beers']);
                }
            });
    }

    finish(): void {
        this.beer.actualStep = 6;
        this.save(true);
    }

    dateChanged() {
        this.beer.bottlingDate = this.bottlingProcess.date;
    }

    goToProcess(process): void {
        if (this.beer.actualStep.valueOf() >= process)
        {
            this._router.navigate(['app/beers/' + process, this.beerID])
        }
    }

    calculateSugar(): void {
        if (this.bottlingProcess.quantity && this.bottlingProcess.coLevel && this.bottlingProcess.grainTemp)
        {
            this.sugarQuantity = Math.round(15.195 * this.bottlingProcess.quantity * 0.264172 * (this.bottlingProcess.coLevel - 3.0378 + (0.050062 * ((this.bottlingProcess.grainTemp * 1.8) + 32)) - (0.00026555 * (((this.bottlingProcess.grainTemp * 1.8) + 32) * ((this.bottlingProcess.grainTemp * 1.8) + 32)))))
            this.glucoseQuantity = Math.round(this.sugarQuantity/0.91);
        }
    }

}
