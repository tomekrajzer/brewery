import { Component, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { BeerServiceProxy, BeerDto, CreateBeerDto, PagedResultDtoOfBeerDto, BeerDtoActualStep} from '@shared/service-proxies/service-proxies';
import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import { Router } from '@angular/router'
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './beer.component.html',
    animations: [appModuleAnimation()],
})
export class BeerComponent extends PagedListingComponentBase<BeerDto> {
    beers: BeerDto[] = [];
    steps = BeerDtoActualStep;

    constructor(
        injector: Injector,
        private _router: Router,
        private _beerService: BeerServiceProxy,
    ) {
        super(injector);
    }

    protected list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
        this._beerService.getAll('Id', request.skipCount, request.maxResultCount)
            .pipe(finalize(() => {
                 finishedCallback()
            }))
            .subscribe((result: PagedResultDtoOfBeerDto) => {
                this.beers = result.items;
                this.showPaging(result, pageNumber);
            });
    }

    protected delete(beer: BeerDto): void {
      abp.message.confirm(
          "Delete beer '" + beer.beerRecipe.name + "'?",
          (result: boolean) => {
              if (result) {
                  this._beerService.delete(beer.id)
                      .subscribe(() => {
                          abp.notify.info("Deleted recipe: " + beer.beerRecipe.name);
                          this.refresh();
                      });
              }
          }
      );
    }

    goToCurrentStage(index){
        var stage = this.beers[index].actualStep == 6? 1 : this.beers[index].actualStep;
        this._router.navigate(['app/beers/' + stage, this.beers[index].id])
    }

    roundSRM(srm) {
        return Math.round(srm);
    }

    protected remove(beer: BeerDto): void {
        abp.message.confirm(
            "Delete beer '" + beer.name + "'?",
            (result: boolean) => {
                if (result) {
                    this._beerService.delete(beer.id)
                        .subscribe(() => {
                            abp.notify.info("Deleted beer: " + beer.name);
                            this.refresh();
                        });
                }
            }
        );
    }
}
