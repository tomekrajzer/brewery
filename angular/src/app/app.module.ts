import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';
import { HttpClientModule, HttpResponse } from '@angular/common/http';

import { ModalModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import {TableModule} from 'ngx-easy-table';
import { TypeaheadModule, ProgressbarModule } from 'ngx-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import { NgDatepickerModule } from 'ng2-datepicker';
import { InputTextModule } from 'primeng/components/inputtext/inputtext'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AbpModule } from '@abp/abp.module';

import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';

import { HomeComponent } from '@app/home/home.component';
import { AboutComponent } from '@app/about/about.component';
import { UsersComponent } from '@app/users/users.component';
import { CreateUserComponent } from '@app/users/create-user/create-user.component';
import { EditUserComponent } from './users/edit-user/edit-user.component';
import { RolesComponent } from '@app/roles/roles.component';
import { BeerStylesComponent } from '@app/beerstyles/beerstyles.component';
import { FermentableIngredientsComponent } from '@app/fermentableIngredients/fermentableIngredients.component';
import { CreateIngredientsComponent } from '@app/fermentableIngredients/create-ingredients/create-ingredients.component';
import { CreateRoleComponent } from '@app/roles/create-role/create-role.component';
import { EditRoleComponent } from './roles/edit-role/edit-role.component';
import { TenantsComponent } from '@app/tenants/tenants.component';
import { CreateTenantComponent } from './tenants/create-tenant/create-tenant.component';
import { EditTenantComponent } from './tenants/edit-tenant/edit-tenant.component';
import { TopBarComponent } from '@app/layout/topbar.component';
import { TopBarLanguageSwitchComponent } from '@app/layout/topbar-languageswitch.component';
import { SideBarUserAreaComponent } from '@app/layout/sidebar-user-area.component';
import { SideBarNavComponent } from '@app/layout/sidebar-nav.component';
import { SideBarFooterComponent } from '@app/layout/sidebar-footer.component';
import { RightSideBarComponent } from '@app/layout/right-sidebar.component';
import { RecipeComponent } from '@app/recipe/recipe.component';
import { CreateRecipeComponent } from '@app/recipe/create-recipe/create-recipe.component';
import { EditRecipeComponent } from '@app/recipe/edit-recipe/edit-recipe.component';
import { BeerComponent } from '@app/beer/beer.component';
import { BeerMashingComponent } from '@app/beer/mashing/beer-mashing.component';
import { BeerBoilingComponent } from '@app/beer/boiling/beer-boiling.component';
import { BeerFermentationComponent } from '@app/beer/fermentation/beer-fermentation.component';
import { QuietFermentationComponent } from '@app/beer/quiet-fermentation/quiet-fermentation.component'
import { BottlingComponent } from '@app/beer/bottling/bottling.component'
import { WarehouseComponent } from '@app/warehouse/warehouse.component'

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        AboutComponent,
        TenantsComponent,
		CreateTenantComponent,
		EditTenantComponent,
        UsersComponent,
		CreateUserComponent,
		EditUserComponent,
      	RolesComponent,
		CreateRoleComponent,
		EditRoleComponent,
        TopBarComponent,
        TopBarLanguageSwitchComponent,
        SideBarUserAreaComponent,
        SideBarNavComponent,
        SideBarFooterComponent,
        RightSideBarComponent,
        BeerStylesComponent,
        FermentableIngredientsComponent,
        CreateIngredientsComponent,
        RecipeComponent,
        CreateRecipeComponent,
        EditRecipeComponent,
        BeerComponent,
        BeerMashingComponent,
        BeerBoilingComponent,
        QuietFermentationComponent,
        BeerFermentationComponent,
        BottlingComponent,
        WarehouseComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        JsonpModule,
        ModalModule.forRoot(),
        TypeaheadModule.forRoot(),
        ProgressbarModule.forRoot(),
        TranslateModule.forRoot(),
        NgDatepickerModule,
        AbpModule,
        TableModule,
        AppRoutingModule,
        ServiceProxyModule,
        SharedModule,
        NgxPaginationModule,
        InputTextModule
    ],
    providers: [
    ]
})
export class AppModule { }
