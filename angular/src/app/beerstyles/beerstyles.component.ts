import { Component, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { BeerStylesServiceProxy, BeerStylesDto, PagedResultDtoOfBeerStylesDto } from '@shared/service-proxies/service-proxies';
import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import { finalize } from 'rxjs/operators';
import { ConfigService } from '../ngx-configuration/configuration.service';

@Component({
    templateUrl: './beerstyles.component.html',
    animations: [appModuleAnimation()],
    providers: [ConfigService]
})
export class BeerStylesComponent extends PagedListingComponentBase<BeerStylesDto> {

    active: boolean = false;
    styles: BeerStylesDto[] = [];
    configuration;
    columns = [
        { key: 'name', title: 'Name', orderEnabled: true },
        { key: 'ibUmin', title: 'IBU' },
        { key: 'ibUmin', title: 'ABV' },
        { key: 'abVmin', title: 'SRM' },
        { key: 'srMmin', title: 'FG' },
        { key: 'oGmin', title: 'OG' },
      ];

    constructor(
        injector: Injector,
        private _beerStyleService: BeerStylesServiceProxy
    ) {
        super(injector);
        this.configuration = ConfigService.config;
    }

    protected list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
        this._beerStyleService.getAll(request.skipCount, 10000)
            .pipe(finalize(() => {
                 finishedCallback()
            }))
            .subscribe((result: PagedResultDtoOfBeerStylesDto) => {
                this.styles = result.items;
                // this.showPaging(result, pageNumber);
            });
    }

    protected delete(style: BeerStylesDto): void {
        abp.message.confirm(
            "Delete beer style '" + style.name + "'?",
            (result: boolean) => {
                if (result) {
                    this._beerStyleService.delete(style.id)
                        .subscribe(() => {
                            abp.notify.info("Deleted style: " + style.name);
                            this.refresh();
                        });
                }
            }
        );
    }
}
