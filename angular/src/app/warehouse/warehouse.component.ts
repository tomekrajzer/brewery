import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { WarehouseDto, WarehouseHopDto, WarehouseIngredientDto, FermentablesIngredientDto, FermentablesIngredientsServiceProxy,
     WarehouseHopsServiceProxy, WarehouseIngredientsServiceProxy, FermentablesIngredient, WarehouseServiceProxy, IngredientUsedForRecipeDto,
     FermentablesIngredientDtoIngredientType, HopDto, Hop, HopsServiceProxy } from '@shared/service-proxies/service-proxies';
import { Router } from '@angular/router'
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { NgForm } from '@angular/forms';
import { elementEnd } from '@angular/core/src/render3';

@Component({
    templateUrl: './warehouse.component.html',
    animations: [appModuleAnimation()],
})
export class WarehouseComponent extends AppComponentBase implements OnInit {
    active: boolean = false;
    saving: boolean = false;
    warehouse: WarehouseDto;
    allIngredients: FermentablesIngredientDto[] = [];
    ingredientTypes: {id: number; name: string}[] = [];
    hops: Hop[] = [];

    constructor(
        injector: Injector,
        private _router: Router,
        private _warehouseService: WarehouseServiceProxy,
        private _ingredientsService: FermentablesIngredientsServiceProxy,
        private _werhouseIngredientService: WarehouseIngredientsServiceProxy,
        private _hopsService: HopsServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.initIngredients();
        this.initHops();
        this.getWarehouseItems();

        for(var n in FermentablesIngredientDtoIngredientType) {
            if (typeof FermentablesIngredientDtoIngredientType[n] === 'number') {
                this.ingredientTypes.push({id: <any>FermentablesIngredientDtoIngredientType[n], name: n});
            }
        }
    }

    getWarehouseItems(): void {
        this._warehouseService.getWarehoseItems()
        .subscribe((result: WarehouseDto)=>{
            result.ingredients.forEach(element => element.quantity = +(element.quantity).toFixed(2));
            this.warehouse = result;

            if(this.warehouse.ingredients.length == 0)
            {
                this.warehouse.ingredients = [];
                var tmpIngredient = new WarehouseIngredientDto();
                var tmpFermentableIngredient = new FermentablesIngredient();
                tmpIngredient.ingredient = tmpFermentableIngredient;
                this.warehouse.ingredients.push(tmpIngredient);
            }
            if(this.warehouse.hops.length == 0)
            {
                this.warehouse.hops = [];
                var tmpWarehouseHop = new WarehouseHopDto();
                var tmpHop = new HopDto();
                tmpWarehouseHop.hop = tmpHop;
                this.warehouse.hops.push(tmpWarehouseHop);
            }
            this.active = true;
        });
    }

    initIngredients(): void {
        this._ingredientsService.getAllIngredients()
            .subscribe((result: FermentablesIngredientDto[])=>{
                this.allIngredients = result;
            });
    }

    ingredientsWarehouseChanged(event, index){
        this.warehouse.ingredients[index].ingredientID = event.item.id;
        this.warehouse.ingredients[index].ebc = event.item.ebc;
        this.warehouse.ingredients[index].yield = event.item.yield;
        this.warehouse.ingredients[index].ingredient = event.item;
    }

    getIngredientTypeValue(ingredient: FermentablesIngredientDto) : string {

        if (ingredient.ingredientType)
        {
            return  this.ingredientTypes.find(i => i.id === ingredient.ingredientType).name
        }
        return undefined;
    }

    save(): void {
        if (!this.warehouse.ingredients[this.warehouse.ingredients.length-1].ingredientID)
        {
            this.warehouse.ingredients.splice(this.warehouse.ingredients.length-1,1);
        }
        if (!this.warehouse.hops[this.warehouse.hops.length-1].hopID)
        {
            this.warehouse.hops.splice(this.warehouse.hops.length-1,1);
        }
        this.saving = true;
        this._warehouseService.saveIngredients(this.warehouse)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
            });
    }

    addIngredient() {
        var tmpIngredient = new WarehouseIngredientDto();
        var tmpFermentableIngredient = new FermentablesIngredient();
        tmpIngredient.ingredient = tmpFermentableIngredient;
        this.warehouse.ingredients.push(tmpIngredient);
    }

    removeIngredient(rowIndex: number): void {
        this._werhouseIngredientService.delete(this.warehouse.ingredients[rowIndex].id)
            .subscribe(() => { });
        this.warehouse.ingredients.splice(rowIndex, 1);
    }

    addHop() {
        var tmpWarehouseHop = new WarehouseHopDto();
        var tmpHop = new HopDto();
        tmpWarehouseHop.hop = tmpHop;
        this.warehouse.hops.push(tmpWarehouseHop);
    }

    initHops(): void {
        this._hopsService.getAllHops()
            .subscribe((result: Hop[])=>{
                this.hops = result;
            });
    }

    removeHop(rowIndex: number): void {
        this._hopsService.delete(this.warehouse.hops[rowIndex].id)
            .subscribe(() => { });
        this.warehouse.hops.splice(rowIndex, 1);
    }

    hopSelected(event, index)
    {
        this.warehouse.hops[index].alphaAcid = event.item.alphaAcids;
        this.warehouse.hops[index].hopID = event.item.id;
        this.warehouse.hops[index].hop = event.item;
    }
}
