import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AbpHttpInterceptor } from '@abp/abpHttpInterceptor';

import * as ApiServiceProxies from './service-proxies';

@NgModule({
    providers: [
        ApiServiceProxies.RoleServiceProxy,
        ApiServiceProxies.SessionServiceProxy,
        ApiServiceProxies.TenantServiceProxy,
        ApiServiceProxies.UserServiceProxy,
        ApiServiceProxies.BeerStylesServiceProxy,
        ApiServiceProxies.BeerRecipeServiceProxy,
        ApiServiceProxies.FermentablesIngredientsServiceProxy,
        ApiServiceProxies.HopsUsedForRecipeServiceProxy,
        ApiServiceProxies.HopsServiceProxy,
        ApiServiceProxies.MashServiceProxy,
        ApiServiceProxies.IngredientsUsedForRecipeServiceProxy,
        ApiServiceProxies.HopsUsedForRecipeServiceProxy,
        ApiServiceProxies.BeerServiceProxy,
        ApiServiceProxies.TokenAuthServiceProxy,
        ApiServiceProxies.AccountServiceProxy,
        ApiServiceProxies.MashStepsServiceProxy,
        ApiServiceProxies.ConfigurationServiceProxy,
        ApiServiceProxies.BoilServiceProxy,
        ApiServiceProxies.FermentationServiceProxy,
        ApiServiceProxies.QuietFermentationServiceProxy,
        ApiServiceProxies.BottlingServiceProxy,
        ApiServiceProxies.WarehouseIngredientsServiceProxy,
        ApiServiceProxies.WarehouseHopsServiceProxy,
        ApiServiceProxies.WarehouseServiceProxy,
        { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true }
    ]
})
export class ServiceProxyModule { }
