import { BreweryTemplatePage } from './app.po';

describe('Brewery App', function() {
  let page: BreweryTemplatePage;

  beforeEach(() => {
    page = new BreweryTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
