﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Brewery.Authorization;
using Brewery.FermentablesIngredients.Dto;
using Brewery.Hops.Dto;
using Brewery.IngredientTypes;
using Brewery.MashProcess.Dto;
using Microsoft.EntityFrameworkCore;

namespace Brewery.MashProcess
{
    [AbpAuthorize(PermissionNames.Pages_Beers)]
    public class MashStepsAppService : AsyncCrudAppService<MashSteps, MashStepDto, long, PagedResultRequestDto, MashStepDto, MashStepDto>, IMashStepsAppService
    {

        public MashStepsAppService(
            IRepository<MashSteps, long> repository)
            : base(repository)
        {

        }

        public async Task InsertOrUpdate(List<MashStepDto> steps)
        {
            foreach (var step in steps)
            {
                var model = new MashSteps()
                {
                    StepOrder = step.StepOrder,
                    Temperature = step.Temperature,
                    TimeInMin = step.TimeInMin,
                    Id = step.Id,
                    BeerID = step.BeerID
                };
                await Repository.InsertOrUpdateAsync(model);
            }
        }

        public async Task<List<MashStepDto>> GetByBeer(long id)
        {
            var result = await Repository.GetAll().Where(x => x.BeerID == id).ToListAsync();
            return ObjectMapper.Map<List<MashStepDto>>(result);
        }
    }
  
}
