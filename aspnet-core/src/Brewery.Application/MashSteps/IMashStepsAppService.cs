﻿using System;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.Hops.Dto;
using Brewery.MashProcess.Dto;

namespace Brewery.MashProcess
{
    public interface IMashStepsAppService : IAsyncCrudAppService<MashStepDto, long, PagedResultRequestDto, MashStepDto, MashStepDto>
    {

    }
}
