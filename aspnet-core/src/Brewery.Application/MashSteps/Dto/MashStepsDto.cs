﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.BeerRecipes.Dto;
using Brewery.IngredientTypes;

namespace Brewery.MashProcess.Dto
{
    [AutoMapTo(typeof(MashSteps))]
    public class MashStepDto : EntityDto<long>
    {
        [Required]
        public int TimeInMin { get; set; }

        [Required]
        public int Temperature { get; set; }

        [Required]
        public int StepOrder { get; set; }

        [Required]
        public long? BeerID { get; set; }

    }
}
