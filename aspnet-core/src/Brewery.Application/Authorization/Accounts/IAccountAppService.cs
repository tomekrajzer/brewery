﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Brewery.Authorization.Accounts.Dto;

namespace Brewery.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
