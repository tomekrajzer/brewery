﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Brewery.Authorization;
using Brewery.BoilProcess.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.Hops.Dto;
using Brewery.IngredientTypes;
using Brewery.MashProcess.Dto;

namespace Brewery.BoilProcess
{
    [AbpAuthorize(PermissionNames.Pages_BeerRecipes)]
    public class BoilAppService : AsyncCrudAppService<Boil, BoilDto, long, PagedResultRequestDto, BoilDto, BoilDto>, IBoilAppService
    {

        public BoilAppService(
            IRepository<Boil, long> repository)
            : base(repository)
        {

        }

        public async Task<BoilDto> GetByBeer(long id)
        {
            var result = await Repository.FirstOrDefaultAsync(x => x.BeerID == id);
            return ObjectMapper.Map<BoilDto>(result);
        }

        public async Task InsertOrUpdate(BoilDto boil)
        {
            var model = new Boil()
            {
                BeerID = boil.BeerID,
                Date = boil.Date,
                BLG = boil.BLG,
                BoilWortSize = boil.BoilWortSize,
                EvaporationRate = boil.EvaporationRate,
                Note = boil.Note,
                Id = boil.Id
            };
            await Repository.InsertOrUpdateAsync(model);
        }

    }
}
