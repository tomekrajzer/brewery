﻿using System;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.BoilProcess.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.Hops.Dto;
using Brewery.MashProcess.Dto;

namespace Brewery.BoilProcess
{
    public interface IBoilAppService : IAsyncCrudAppService<BoilDto, long, PagedResultRequestDto, BoilDto, BoilDto>
    {

    }
}
