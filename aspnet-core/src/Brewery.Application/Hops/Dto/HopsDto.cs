﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Brewery.IngredientTypes;

namespace Brewery.Hops.Dto
{
    [AutoMapTo(typeof(Hop))]
    public class HopDto : EntityDto<int>
    {
        [Required]
        public double AlphaAcids { get; set; }

        [Required]
        public string Name { get; set; }

    }
}
