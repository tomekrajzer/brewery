﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Brewery.Authorization;
using Brewery.FermentablesIngredients.Dto;
using Brewery.Hops.Dto;
using Brewery.IngredientTypes;

namespace Brewery.Hops
{
    [AbpAuthorize(PermissionNames.Pages_Hops)]
    public class HopsAppService : AsyncCrudAppService<Hop, HopDto, int, PagedResultRequestDto, HopDto, HopDto>, IHopsAppService
    {

        public HopsAppService(
            IRepository<Hop, int> repository)
            : base(repository)
        {

        }

        public async Task<List<Hop>> GetAllHops()
        {
            return await Repository.GetAllListAsync();
        }

    }
}
