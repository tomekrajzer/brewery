﻿using System;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.Hops.Dto;

namespace Brewery.Hops
{
    public interface IHopsAppService : IAsyncCrudAppService<HopDto, int, PagedResultRequestDto, HopDto, HopDto>
    {

    }
}
