﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Brewery.Authorization;
using Brewery.BoilProcess.Dto;
using Brewery.BottlingProcess.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.FermentationProcess.Dto;
using Brewery.Hops.Dto;
using Brewery.IngredientTypes;
using Brewery.MashProcess.Dto;

namespace Brewery.BottlingProcess
{
    [AbpAuthorize(PermissionNames.Pages_BeerRecipes)]
    public class BottlingAppService : AsyncCrudAppService<Bottling, BottlingDto, long, PagedResultRequestDto, BottlingDto, BottlingDto>, IBottlingAppService
    {

        public BottlingAppService(
            IRepository<Bottling, long> repository)
            : base(repository)
        {

        }

        public async Task<BottlingDto> GetByBeer(long id)
        {
            var result = await Repository.FirstOrDefaultAsync(x => x.BeerID == id);
            return ObjectMapper.Map<BottlingDto>(result);
        }

        public async Task InsertOrUpdate(BottlingDto bottling)
        {
            var model = new Bottling()
            {
                BeerID = bottling.BeerID,
                Date = bottling.Date,
                BLG = bottling.BLG,
                Quantity = bottling.Quantity,
                COLevel = bottling.COLevel,
                GrainTemp = bottling.GrainTemp,
                Id = bottling.Id
            };
            await Repository.InsertOrUpdateAsync(model);
        }

        public DateTime? GetBottlingDate(long id)
        {
            return Repository.FirstOrDefaultAsync(x => x.BeerID == id).Result.Date;
        }

    }
}
