﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.BeerRecipes.Dto;
using Brewery.Beers;
using Brewery.IngredientTypes;

namespace Brewery.BottlingProcess.Dto
{
    [AutoMapTo(typeof(Bottling))]
    public class BottlingDto : EntityDto<long>
    {
        public long? BeerID { get; set; }

        public DateTime? Date { get; set; }

        public double? BLG { get; set; }

        public double? Quantity { get; set; }

        public double? COLevel { get; set; }

        public double? GrainTemp { get; set; }
    }
}
