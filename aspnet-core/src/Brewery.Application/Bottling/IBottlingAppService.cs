﻿using System;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.BoilProcess.Dto;
using Brewery.BottlingProcess.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.FermentationProcess.Dto;
using Brewery.Hops.Dto;
using Brewery.MashProcess.Dto;

namespace Brewery.BottlingProcess
{
    public interface IBottlingAppService : IAsyncCrudAppService<BottlingDto, long, PagedResultRequestDto, BottlingDto, BottlingDto>
    {

    }
}
