﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Brewery.Sessions.Dto;

namespace Brewery.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
