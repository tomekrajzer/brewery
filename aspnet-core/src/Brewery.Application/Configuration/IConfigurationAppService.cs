﻿using System.Threading.Tasks;
using Brewery.Configuration.Dto;

namespace Brewery.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
