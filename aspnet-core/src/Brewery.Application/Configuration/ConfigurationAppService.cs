﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using Brewery.Configuration.Dto;

namespace Brewery.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : BreweryAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
