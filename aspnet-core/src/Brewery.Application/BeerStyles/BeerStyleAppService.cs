﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Localization;
using Abp.Runtime.Session;
using Brewery.Authorization;
using Brewery.Authorization.Roles;
using Brewery.Authorization.Users;
using Brewery.Roles.Dto;
using Brewery.Users.Dto;
using Brewery.BeerStyles.Dto;
using Abp.Dependency;
using System.IO;
using CsvHelper;
using System.Text;
using System;

namespace Brewery.BeerStyles
{
    [AbpAuthorize(PermissionNames.Pages_BeerStyles)]
    public class BeerStylesAppService : AsyncCrudAppService<BeerStyleParams, BeerStylesDto, int, PagedResultRequestDto, CreateBeerStylesDto, BeerStylesDto>, IBeerStylesAppService
    {

        public BeerStylesAppService(
            IRepository<BeerStyleParams, int> repository)
            : base(repository)
        {

        }

        public async Task ImportDataFromCSV()
        {
            try
            {
                var csv = new CsvReader(File.OpenText("/Users/TOMEK/Repositories/Brewery/beerStyle.csv"));
                csv.Configuration.MissingFieldFound = null;
                // Turn off header validation.
                csv.Configuration.HeaderValidated = null;

                // Log instead of throwing an exception.
                csv.Configuration.HeaderValidated = (isValid, headerNames, headerNameIndex, context) =>
                {
                    if (!isValid)
                    {
                        Console.WriteLine($"Header matching ['{string.Join("', '", headerNames)}'] names at index {headerNameIndex} was not found.");
                    }
                };
                var records = csv.GetRecords<BeerStyleParams>().ToList();

                foreach (var record in records)
                {
                    Console.WriteLine(record.Name);
                    //await Repository.InsertAsync(record);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception from ImportDataFromCSV");
            }
        }

        public async Task<List<BeerStyleParams>> GetAllStyles()
        {
            return await Repository.GetAllListAsync();
        }


    }
}