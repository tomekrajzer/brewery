using System.ComponentModel.DataAnnotations;
using Abp.Auditing;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using Brewery.Authorization.Users;

namespace Brewery.BeerStyles.Dto
{
    [AutoMapTo(typeof(BeerStyleParams))]
    public class CreateBeerStylesDto
    {
        [Required]
        public double OGmin { get; set; }

        [Required]
        public double OGmax { get; set; }

        [Required]
        public double FGmin { get; set; }

        [Required]
        public double FGmax { get; set; }

        [Required]
        public double ABVmin { get; set; }

        [Required]
        public double ABVmax { get; set; }

        [Required]
        public double IBUmin { get; set; }

        [Required]
        public double IBUmax { get; set; }

        [Required]
        public double SRMmin { get; set; }

        [Required]
        public double SRMmax { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
