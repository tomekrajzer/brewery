using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.BeerStyles.Dto;
using Brewery.Roles.Dto;
using Brewery.Users.Dto;

namespace Brewery.BeerStyles
{
    public interface IBeerStylesAppService : IAsyncCrudAppService<BeerStylesDto, int, PagedResultRequestDto, CreateBeerStylesDto, BeerStylesDto>
    {

    }
}
