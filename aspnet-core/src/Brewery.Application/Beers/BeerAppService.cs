﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.Dependency;
using System.IO;
using CsvHelper;
using System.Text;
using System;
using Brewery.BeerRecipes.Dto;
using Brewery.Authorization;
using Brewery.FermentablesIngredients;
using Brewery.HopsUsedForRecipe;
using Brewery.Hops;
using Brewery.MashProcess;
using Brewery.IngredientsUsedForRecipe;
using Brewery.IngredientsUsedForRecipe.Dto;
using Brewery.HopsUsedForRecipe.Dto;
using Brewery.MashProcess.Dto;
using Brewery.Beers.Dto;
using Brewery.BeerRecipes;
using Brewery.BottlingProcess;

namespace Brewery.Beers
{
    [AbpAuthorize(PermissionNames.Pages_Beers)]
    public class BeerAppService : AsyncCrudAppService<Beer, BeerDto, long, PagedAndSortedResultRequestDto, CreateBeerDto, BeerDto>, IBeerAppService
    {
        private readonly IngredientsUsedForRecipeAppService _ingredientUsedService;
        private readonly FermentablesIngredientsAppService _ingredientService;
        private readonly HopsUsedForRecipeAppService _hopsUsedService;
        private readonly HopsAppService _hopsService;
        private readonly MashAppService _mashService;
        private readonly BeerRecipeAppService _beerRecipeService;
        private readonly IAbpSession _session;
        private readonly BottlingAppService _bottlingService;

        public BeerAppService(IngredientsUsedForRecipeAppService ingredientUsedService,
            FermentablesIngredientsAppService ingredientService,
            HopsUsedForRecipeAppService hopsUsedService,
            HopsAppService hopsService,
            MashAppService mashService,
            BeerRecipeAppService beerRecipeService,
            BottlingAppService bottlingService,
            IAbpSession session,
            IRepository<Beer, long> repository)
            : base(repository)
        {
            _ingredientUsedService = ingredientUsedService;
            _ingredientService = ingredientService;
            _hopsUsedService = hopsUsedService;
            _hopsService = hopsService;
            _mashService = mashService;
            _session = session;
            _beerRecipeService = beerRecipeService;
        }

        public async Task<long> CreateBeer(CreateBeerDto input)
        {
            var batch = new Beer()
            {
                UserId = _session.UserId,
                BeerRecipeID = input.BeerRecipeID,
                Name = input.Name,
                ActualStep = input.ActualStep

            };
            return await Repository.InsertAndGetIdAsync(batch);
        }

        public override async Task<BeerDto> Get(EntityDto<long> input)
        {
            var result = await Repository.GetAllIncluding(x => x.BeerRecipe).FirstOrDefaultAsync(x => x.Id == input.Id);
            return ObjectMapper.Map<BeerDto>(result);
        }

        public override async Task<BeerDto> Update(BeerDto input)
        {
            var model = new Beer()
            {
                ActualStep = input.ActualStep,
                BeerNumber = input.BeerNumber,
                BeerRecipeID = input.BeerRecipeID,
                CreationTime = input.CreationTime,
                Id = input.Id,
                Name = input.Name,
                BottlingDate = input.BottlingDate,
                BLG = input.BLG,
                UserId = input.UserId
            };

            var result = await Repository.InsertOrUpdateAsync(model);
            return ObjectMapper.Map<BeerDto>(result);
        }

        public override async Task<PagedResultDto<BeerDto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            var userId = _session.UserId;
            var items = await Repository.GetAllIncluding(x => x.BeerRecipe).Where(x => x.UserId == userId).OrderByDescending(x => x.Id).ToListAsync();
            return new PagedResultDto<BeerDto>(items.Count(), ObjectMapper.Map<IReadOnlyList<BeerDto>>(items));
        }

        public async Task<int> GetBeerNumber()
        {
            var userId = _session.UserId;
            return await Repository.CountAsync(x => x.UserId == userId);
        }
    }
}