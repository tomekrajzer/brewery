using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.BeerRecipes.Dto;
using Brewery.Beers.Dto;
using Brewery.Users.Dto;

namespace Brewery.Beers
{
    public interface IBeerAppService : IAsyncCrudAppService<BeerDto, long, PagedAndSortedResultRequestDto, CreateBeerDto, BeerDto>
    {

    }
}
