using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Brewery.Authorization.Users;
using Brewery.BeerSteps;
using Brewery.BeerStyles;
using Brewery.HopsUsedForRecipe.Dto;
using Brewery.IngredientsUsedForRecipe.Dto;
using Brewery.MashProcess.Dto;

namespace Brewery.Beers.Dto
{
    public class CreateBeerDto
    {
        [Required]
        public BeerStep ActualStep { get; set; }

        [Required]
        public long? BeerRecipeID { get; set; }

        public int? BeerNumber { get; set; }

        public string Name { get; set; }

        public DateTime CreationTime { get; set; }

        public long? UserId { get; set; }
    }
}
