using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Brewery.Authorization.Users;
using Brewery.BeerRecipes;
using Brewery.BeerSteps;
using Brewery.BeerStyles;
using Brewery.MashProcess;

namespace Brewery.Beers.Dto
{
    [AutoMapTo(typeof(Beer))]
    public class BeerDto : EntityDto<long>
    {
        [Required]
        public BeerStep ActualStep { get; set; }

        public BeerRecipe BeerRecipe { get; set; }
        [Required]
        public long? BeerRecipeID { get; set; }

        [Required]
        public int? BeerNumber { get; set; }

        public string Name { get; set; }

        public double? BLG { get; set; }

        public double? Size { get; set; }

        public DateTime CreationTime { get; set; }

        public DateTime? BottlingDate { get; set; }

        public User User { get; set; }
        public long? UserId { get; set; }

    }
}
