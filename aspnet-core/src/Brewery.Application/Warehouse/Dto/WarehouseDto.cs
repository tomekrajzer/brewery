﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.BeerRecipes.Dto;
using Brewery.Beers;
using Brewery.IngredientTypes;
using Brewery.WarehouseHops;
using Brewery.WarehouseHops.Dto;
using Brewery.WarehouseIngredients.Dto;

namespace Brewery.Warehouse.Dto
{
    public class WarehouseDto
    {
        public List<WarehouseHopDto> Hops { get; set; }

        public List<WarehouseIngredientDto> Ingredients { get; set; }
    }
}
