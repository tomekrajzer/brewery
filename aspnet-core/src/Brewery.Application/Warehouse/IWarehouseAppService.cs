﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.Hops.Dto;
using Brewery.MashProcess.Dto;
using Brewery.Warehouse.Dto;
using Brewery.WarehouseHops.Dto;
using Brewery.WarehouseIngredients.Dto;

namespace Brewery.Warehouse
{
    public interface IWarehouseAppService: IApplicationService
    {
        //Task<WarehouseDto> GetWarehoseItems();
    }
}
