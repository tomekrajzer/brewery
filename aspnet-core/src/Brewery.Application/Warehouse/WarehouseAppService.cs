﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Abp.Runtime.Session;
using Brewery.Authorization;
using Brewery.HopsUsedForRecipe;
using Brewery.IngredientsUsedForRecipe;
using Brewery.MashProcess.Dto;
using Brewery.Warehouse.Dto;
using Brewery.WarehouseHops;
using Brewery.WarehouseHops.Dto;
using Brewery.WarehouseIngredients;
using Brewery.WarehouseIngredients.Dto;
using Microsoft.EntityFrameworkCore;

namespace Brewery.Warehouse
{
    [AbpAuthorize(PermissionNames.Pages_Warehouse)]
    public class WarehouseAppService: IWarehouseAppService
    {
        private readonly IIngredientsUsedForRecipeAppService _ingredientsUsedForRecipeAppService;
        private readonly IHopsUsedForRecipeAppService _hopsUsedForRecipeAppService;
        private readonly IWarehouseIngredientsAppService _warehouseIngredient;
        private readonly IWarehouseHopsAppService _warehouseHop;
        private readonly IAbpSession _session;
        private readonly IObjectMapper _objectMapper;

        public WarehouseAppService(IWarehouseHopsAppService warehouseHop, IWarehouseIngredientsAppService warehouseIngredient,
         IAbpSession session, IObjectMapper objectMapper, IIngredientsUsedForRecipeAppService ingredientsUsedForRecipeAppService,
            IHopsUsedForRecipeAppService hopsUsedForRecipeAppService)
        {
            _warehouseHop = warehouseHop;
            _warehouseIngredient = warehouseIngredient;
            _session = session;
            _objectMapper = objectMapper;
            _ingredientsUsedForRecipeAppService = ingredientsUsedForRecipeAppService;
            _hopsUsedForRecipeAppService = hopsUsedForRecipeAppService;
        }

        public async Task<WarehouseDto> GetWarehoseItems()
        {
            var userId = _session.UserId;
            var ingredients = await _warehouseIngredient.GetAllUserIngredient();
            var hops = await _warehouseHop.GetAllUserHops();

            return new WarehouseDto()
            {
                Hops = hops,
                Ingredients = ingredients
            };
        }

        public async Task SaveIngredients(WarehouseDto warehouse)
        {
            foreach (var ingredient in warehouse.Ingredients)
            {
                await _warehouseIngredient.InsertOrUpdateIngredients(ingredient);
            }

            foreach (var hop in warehouse.Hops)
            {
                await _warehouseHop.InsertOrUpdateHops(hop);
            }
        }
    }
}
