﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.MultiTenancy.Dto;

namespace Brewery.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

