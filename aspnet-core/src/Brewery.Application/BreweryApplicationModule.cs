﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Brewery.Authorization;

namespace Brewery
{
    [DependsOn(
        typeof(BreweryCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class BreweryApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<BreweryAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(BreweryApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
