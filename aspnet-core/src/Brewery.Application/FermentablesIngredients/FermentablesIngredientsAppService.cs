﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Brewery.Authorization;
using Brewery.FermentablesIngredients.Dto;
using Brewery.IngredientTypes;

namespace Brewery.FermentablesIngredients
{
    [AbpAuthorize(PermissionNames.Pages_Ingredients)]
    public class FermentablesIngredientsAppService : AsyncCrudAppService<FermentablesIngredient, FermentablesIngredientDto, int, PagedResultRequestDto, FermentablesIngredientDto, FermentablesIngredientDto>, IFermentablesIngredientsAppService
    {

        public FermentablesIngredientsAppService(
            IRepository<FermentablesIngredient, int> repository)
            : base(repository)
        {

        }

        public async Task ImportDataFromXML()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("/Users/TOMEK/Repositories/Brewery/Grain.xml");
                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/DATABASE/FERMENTABLE");
                foreach (XmlNode node in nodes)
                {
                    var ingredient = new FermentablesIngredient
                    {
                        Name = node.SelectSingleNode("NAME").InnerText,
                        Yield = Double.Parse(node.SelectSingleNode("YIELD").InnerText),
                        EBC = Math.Round((Double.Parse(node.SelectSingleNode("EBC").InnerText) * 1.97)), //EBC = SRM * 1.97
                        Supplier = node.SelectSingleNode("SUPPLIER").InnerText,
                        Desscription = node.SelectSingleNode("DESCRIPTION").InnerText,
                        Moisture = Double.Parse(node.SelectSingleNode("MOISTURE").InnerText),
                        Protein = Double.Parse(node.SelectSingleNode("PROTEIN").InnerText),
                        IngredientType = IngredientType.Ziarno
                    };
                    await Repository.InsertAsync(ingredient);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception from ImportDataFromXML");
            }
        }

        public async Task<List<FermentablesIngredientDto>> GetAllIngredients()
        {
            return ObjectMapper.Map<List<FermentablesIngredientDto>>(await Repository.GetAllListAsync());
        }

    }
}
