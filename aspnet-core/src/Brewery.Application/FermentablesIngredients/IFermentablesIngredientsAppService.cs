﻿using System;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.FermentablesIngredients.Dto;

namespace Brewery.FermentablesIngredients
{
    public interface IFermentablesIngredientsAppService : IAsyncCrudAppService<FermentablesIngredientDto, int, PagedResultRequestDto, FermentablesIngredientDto, FermentablesIngredientDto>
    {

    }
}
