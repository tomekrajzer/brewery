﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Brewery.Authorization;
using Brewery.IngredientsUsedForRecipe;
using Brewery.MashProcess.Dto;
using Brewery.WarehouseIngredients;
using Brewery.WarehouseIngredients.Dto;
using Microsoft.EntityFrameworkCore;

namespace Brewery.WarehouseIngredients
{
    [AbpAuthorize(PermissionNames.Pages_Warehouse)]
    public class WarehouseIngredientsAppService : AsyncCrudAppService<WarehouseIngredient, WarehouseIngredientDto, int, PagedResultRequestDto, WarehouseIngredientDto, WarehouseIngredientDto>, IWarehouseIngredientsAppService
    {
        private readonly IAbpSession _session;
        private readonly IIngredientsUsedForRecipeAppService _ingredientsUsedForRecipeAppService;

        public WarehouseIngredientsAppService(
            IRepository<WarehouseIngredient, int> repository, IAbpSession session, IIngredientsUsedForRecipeAppService ingredientsUsedForRecipeAppService)
            : base(repository)
        {
            _session = session;
            _ingredientsUsedForRecipeAppService = ingredientsUsedForRecipeAppService;
        }

        public async Task<List<WarehouseIngredientDto>> GetAllUserIngredient()
        {
            var result = await Repository.GetAllIncluding(x => x.Ingredient).Where(x => x.UserId == _session.UserId).ToListAsync();
            return ObjectMapper.Map<List<WarehouseIngredientDto>>(result);
        }

        public async Task InsertOrUpdateIngredients(WarehouseIngredientDto ingredient)
        {

            var model = new WarehouseIngredient()
            {
                UserId = _session.UserId,
                EBC = ingredient.EBC,
                IngredientID = ingredient.IngredientID,
                Quantity = ingredient.Quantity,
                Yield = ingredient.Yield,
                Id = ingredient.Id
            };

            await Repository.InsertOrUpdateAsync(model);
        }

        public async Task UpdateQuantity(List<WarehouseIngredientDto> ingredients)
        {

            foreach (var ingredient in ingredients)
            {
                var item = ObjectMapper.Map<WarehouseIngredient>(ingredient);
                
                await Repository.UpdateAsync(item);
            }

        }

    }
}
