﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.BeerRecipes.Dto;
using Brewery.Beers;
using Brewery.FermentablesIngredients;
using Brewery.IngredientTypes;
using Brewery.WarehouseHops;

namespace Brewery.WarehouseIngredients.Dto
{
    [AutoMapTo(typeof(WarehouseIngredient))]
    public class WarehouseIngredientDto : EntityDto<int>
    {
        public double Quantity { get; set; }

        public double EBC { get; set; }

        public double Yield { get; set; }

        public int? IngredientID { get; set; }

        public FermentablesIngredient Ingredient { get; set; }

        public long? UserId { get; set; }
    }
}
