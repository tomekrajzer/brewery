﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.Hops.Dto;
using Brewery.MashProcess.Dto;
using Brewery.WarehouseIngredients.Dto;

namespace Brewery.WarehouseIngredients
{
    public interface IWarehouseIngredientsAppService : IAsyncCrudAppService<WarehouseIngredientDto, int, PagedResultRequestDto, WarehouseIngredientDto, WarehouseIngredientDto>
    {
        Task<List<WarehouseIngredientDto>> GetAllUserIngredient();
        Task InsertOrUpdateIngredients(WarehouseIngredientDto ingredient);
        //Task UpdateQuantity(WarehouseIngredientDto ingredient);
    }
}
