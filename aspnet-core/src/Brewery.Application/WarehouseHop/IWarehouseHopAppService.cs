﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.Hops.Dto;
using Brewery.MashProcess.Dto;
using Brewery.WarehouseHops.Dto;
using Brewery.WarehouseIngredients.Dto;

namespace Brewery.WarehouseHops
{
    public interface IWarehouseHopsAppService : IAsyncCrudAppService<WarehouseHopDto, int, PagedResultRequestDto, WarehouseHopDto, WarehouseHopDto>
    {
        Task InsertOrUpdateHops(WarehouseHopDto hop);

        Task<List<WarehouseHopDto>> GetAllUserHops();
    }
}
