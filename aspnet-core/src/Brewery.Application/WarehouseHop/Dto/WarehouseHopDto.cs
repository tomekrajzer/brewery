﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.BeerRecipes.Dto;
using Brewery.Beers;
using Brewery.Hops;
using Brewery.IngredientTypes;
using Brewery.WarehouseHops;

namespace Brewery.WarehouseHops.Dto
{
    [AutoMapTo(typeof(WarehouseHop))]
    public class WarehouseHopDto : EntityDto<int>
    {
        public double Quantity { get; set; }

        public double AlphaAcid { get; set; }

        public int Year { get; set; }

        public int? HopID { get; set; }

        public Hop Hop { get; set; }

        public long? UserId { get; set; }

    }
}
