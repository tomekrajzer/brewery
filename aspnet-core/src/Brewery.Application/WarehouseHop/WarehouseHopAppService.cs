﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Brewery.Authorization;
using Brewery.MashProcess.Dto;
using Brewery.WarehouseHops.Dto;
using Brewery.WarehouseIngredients;
using Brewery.WarehouseIngredients.Dto;
using Microsoft.EntityFrameworkCore;

namespace Brewery.WarehouseHops
{
    [AbpAuthorize(PermissionNames.Pages_Warehouse)]
    public class WarehouseHopsAppService : AsyncCrudAppService<WarehouseHop, WarehouseHopDto, int, PagedResultRequestDto, WarehouseHopDto, WarehouseHopDto>, IWarehouseHopsAppService
    {
        private readonly IAbpSession _session;


        public WarehouseHopsAppService(
            IRepository<WarehouseHop, int> repository, IAbpSession session)
            : base(repository)
        {
            _session = session;
        }

        public async Task InsertOrUpdateHops(WarehouseHopDto hop)
        {

            var model = new WarehouseHop()
            {
                UserId = _session.UserId,
                Quantity = hop.Quantity,
                Id = hop.Id,
                AlphaAcid = hop.AlphaAcid,
                HopID = hop.HopID,
                Year = hop.Year
            };

            await Repository.InsertOrUpdateAsync(model);
        }

        public async Task<List<WarehouseHopDto>> GetAllUserHops()
        {
            var result = await Repository.GetAllIncluding(x => x.Hop).Where(x => x.UserId == _session.UserId).ToListAsync();
            return ObjectMapper.Map<List<WarehouseHopDto>>(result);
        }

        public async Task UpdateQuantity(List<WarehouseHopDto> hops)
        {
            //var zhops = hops.GroupBy(x => x.Id).Select(x => x.Select(z => new WarehouseHopDto { AlphaAcid = z.AlphaAcid, HopID = z.HopID, Id = z.Id, UserId = z.UserId, Year = z.Year, Quantity = x.Sum(c => c.Quantity) })).ToList();

            foreach (var hop in hops)
            {
                var item = ObjectMapper.Map<WarehouseHop>(hop);

                await Repository.UpdateAsync(item);
            }

        }
    }
}
