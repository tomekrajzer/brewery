﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Brewery.Authorization;
using Brewery.FermentablesIngredients.Dto;
using Brewery.IngredientsUsedForRecipe;
using Brewery.IngredientsUsedForRecipe.Dto;
using Brewery.IngredientTypes;
using Microsoft.EntityFrameworkCore;

namespace Brewery.FermentablesIngredients
{
    [AbpAuthorize(PermissionNames.Pages_Ingredients)]
    public class IngredientsUsedForRecipeAppService : AsyncCrudAppService<IngredientUsedForRecipe, IngredientUsedForRecipeDto, long, PagedResultRequestDto, CreateIngredientUsedForRecipeDto, IngredientUsedForRecipeDto>, IIngredientsUsedForRecipeAppService
    {

        private readonly FermentablesIngredientsAppService _ingredientService;

        public IngredientsUsedForRecipeAppService(
            FermentablesIngredientsAppService ingredientService,
            IRepository<IngredientUsedForRecipe, long> repository)
            : base(repository)
        {
            _ingredientService = ingredientService;
        }

        public async Task<long> Create2(IngredientUsedForRecipeDto input)
        {
            var item = new IngredientUsedForRecipe()
            {
                BeerRecipeID = input.BeerRecipeID,
                CustomEBC = input.CustomEBC,
                CustomYield = input.CustomYield,
                IngredientID = input.IngredientID,
                PercentageSize = input.PercentageSize,
                Quantity = input.Quantity
            };

            return await Repository.InsertAndGetIdAsync(item);
        }

        public async Task<IngredientUsedForRecipe> UpdateIngredient(IngredientUsedForRecipeDto input)
        {
            var item = new IngredientUsedForRecipe()
            {
                BeerRecipeID = input.BeerRecipeID,
                CustomEBC = input.CustomEBC,
                CustomYield = input.CustomYield,
                IngredientID = input.IngredientID,
                PercentageSize = input.PercentageSize,
                Quantity = input.Quantity,
                Id = input.Id
            };

            return await Repository.InsertOrUpdateAsync(item);
        }

        public async Task<List<IngredientUsedForRecipeDto>> GetRecipeIngredients(long id)
        {
            var result = await Repository.GetAllIncluding(x => x.Ingredient).Where(x => x.BeerRecipeID == id).ToListAsync();
            return ObjectMapper.Map<List<IngredientUsedForRecipeDto>>(result);
        }

        public async Task<double> CalculateWaterToMash(long id, double ratio)
        {
            var sizeIngedient = 0.0;
            var result = await Repository.GetAllIncluding(x => x.Ingredient).Where(x => x.BeerRecipeID == id).ToListAsync();
            foreach (var item in result)
            {
                sizeIngedient += item.Quantity;
            }

            return sizeIngedient * ratio;
        }
    }
}
