﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.IngredientsUsedForRecipe.Dto;

namespace Brewery.IngredientsUsedForRecipe
{
    public interface IIngredientsUsedForRecipeAppService : IAsyncCrudAppService<IngredientUsedForRecipeDto, long, PagedResultRequestDto, CreateIngredientUsedForRecipeDto, IngredientUsedForRecipeDto>
    {
        Task<long> Create2(IngredientUsedForRecipeDto input);
        Task<List<IngredientUsedForRecipeDto>> GetRecipeIngredients(long id);
    }
}
