﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Brewery.FermentablesIngredients.Dto;

namespace Brewery.IngredientsUsedForRecipe.Dto
{
    public class CreateIngredientUsedForRecipeDto
    {
        public long? BeerRecipeID { get; set; }

        [Required]
        public double Quantity { get; set; }

        [Required]
        public double PercentageSize { get; set; }

        [Required]
        public int? IngredientID { get; set; }

        public double CustomYield { get; set; }

        public double CustomEBC { get; set; }
    }
}
