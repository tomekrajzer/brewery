﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.FermentablesIngredients;
using Brewery.FermentablesIngredients.Dto;
using Brewery.IngredientTypes;

namespace Brewery.IngredientsUsedForRecipe.Dto
{
    [AutoMapTo(typeof(IngredientUsedForRecipe))]
    public class IngredientUsedForRecipeDto : EntityDto<long>
    {
        public long? BeerRecipeID { get; set; }
        public BeerRecipe BeerRecipe { get; set; }

        [Required]
        public double Quantity { get; set; }

        [Required]
        public double PercentageSize { get; set; }

        [Required]
        public int? IngredientID { get; set; }

        public FermentablesIngredient Ingredient { get; set; }

        public double CustomYield { get; set; }

        public double CustomEBC { get; set; }

    }
}
