﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.BeerRecipes.Dto;
using Brewery.Beers;
using Brewery.IngredientTypes;

namespace Brewery.MashProcess.Dto
{
    [AutoMapTo(typeof(Mash))]
    public class MashDto : EntityDto<long>
    {
        public long? BeerID { get; set; }

        public DateTime? Date { get; set; }

        public string Note { get; set; }

        public double? LauteringTemp { get; set; }

        public double? GrainTemp { get; set; }

        public double? WaterQuantityToMash { get; set; }

    }
}
