﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Brewery.Authorization;
using Brewery.FermentablesIngredients.Dto;
using Brewery.Hops.Dto;
using Brewery.IngredientsUsedForRecipe;
using Brewery.IngredientTypes;
using Brewery.MashProcess.Dto;
using Brewery.Warehouse;
using Brewery.WarehouseIngredients;

namespace Brewery.MashProcess
{
    [AbpAuthorize(PermissionNames.Pages_BeerRecipes)]
    public class MashAppService : AsyncCrudAppService<Mash, MashDto, long, PagedResultRequestDto, MashDto, MashDto>, IMashAppService
    {

        private readonly IIngredientsUsedForRecipeAppService _ingredientsUsedForRecipeAppService;
        private readonly IWarehouseAppService _warehouseAppService;

        public MashAppService(
            IRepository<Mash, long> repository, IIngredientsUsedForRecipeAppService ingredientsUsedForRecipeAppService,
            IWarehouseAppService warehouseAppService)
            : base(repository)
        {
            _ingredientsUsedForRecipeAppService = ingredientsUsedForRecipeAppService;
            _warehouseAppService = warehouseAppService;
        }

        public override Task<MashDto> Update(MashDto input)
        {
            return base.Update(input);
        }

        public async Task InsertOrUpdate(MashDto mash)
        {
            var model = new Mash()
            {
                BeerID = mash.BeerID,
                Date = mash.Date,
                GrainTemp = mash.GrainTemp,
                LauteringTemp = mash.LauteringTemp,
                WaterQuantityToMash = mash.WaterQuantityToMash,
                Note = mash.Note,
                Id = mash.Id
            };
            await Repository.InsertOrUpdateAsync(model);
        }

        public async Task<MashDto> GetByBeer(long id)
        {
            var result = await Repository.FirstOrDefaultAsync(x => x.BeerID == id);
            return ObjectMapper.Map<MashDto>(result);
        }

    }
}
