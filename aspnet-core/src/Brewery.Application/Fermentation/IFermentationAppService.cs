﻿using System;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.BoilProcess.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.FermentationProcess.Dto;
using Brewery.Hops.Dto;
using Brewery.MashProcess.Dto;

namespace Brewery.FermentationProcess
{
    public interface IFermentationAppService : IAsyncCrudAppService<FermentationDto, long, PagedResultRequestDto, FermentationDto, FermentationDto>
    {

    }
}
