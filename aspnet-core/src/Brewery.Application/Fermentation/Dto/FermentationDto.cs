﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.BeerRecipes.Dto;
using Brewery.Beers;
using Brewery.IngredientTypes;

namespace Brewery.FermentationProcess.Dto
{
    [AutoMapTo(typeof(Fermentation))]
    public class FermentationDto : EntityDto<long>
    {
        public long? BeerID { get; set; }

        public DateTime? Date { get; set; }

        public double? BLG { get; set; }

        public string Note { get; set; }

        public double? WortSize { get; set; }

        public double? BoilingLosses { get; set; }
    }
}
