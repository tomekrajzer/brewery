﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Brewery.Authorization;
using Brewery.BoilProcess.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.FermentationProcess.Dto;
using Brewery.Hops.Dto;
using Brewery.IngredientTypes;
using Brewery.MashProcess.Dto;

namespace Brewery.FermentationProcess
{
    [AbpAuthorize(PermissionNames.Pages_BeerRecipes)]
    public class FermentationAppService : AsyncCrudAppService<Fermentation, FermentationDto, long, PagedResultRequestDto, FermentationDto, FermentationDto>, IFermentationAppService
    {

        public FermentationAppService(
            IRepository<Fermentation, long> repository)
            : base(repository)
        {

        }

        public async Task<FermentationDto> GetByBeer(long id)
        {
            var result = await Repository.FirstOrDefaultAsync(x => x.BeerID == id);
            return ObjectMapper.Map<FermentationDto>(result);
        }

        public async Task InsertOrUpdate(FermentationDto fermentation)
        {
            var model = new Fermentation()
            {
                BeerID = fermentation.BeerID,
                Date = fermentation.Date,
                BLG = fermentation.BLG,
                WortSize = fermentation.WortSize,
                Note = fermentation.Note,
                Id = fermentation.Id,
                BoilingLosses = fermentation.BoilingLosses
            };
            await Repository.InsertOrUpdateAsync(model);
        }

    }
}
