﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Brewery.Authorization;
using Brewery.HopsUsedForRecipe;
using Brewery.HopsUsedForRecipe.Dto;
using Microsoft.EntityFrameworkCore;

namespace Brewery.HopsUsedForRecipe
{
    [AbpAuthorize(PermissionNames.Pages_Hops)]
    public class HopsUsedForRecipeAppService : AsyncCrudAppService<HopUsedForRecipe, HopsUsedForRecipeDto, long, PagedResultRequestDto, CreateHopsUsedForRecipeDto, HopsUsedForRecipeDto>, IHopsUsedForRecipeAppService
    {

        public HopsUsedForRecipeAppService(
            IRepository<HopUsedForRecipe, long> repository)
            : base(repository)
        {

        }

        public async Task<long> Create2(HopsUsedForRecipeDto input)
        {
            var hopUsed = new HopUsedForRecipe()
            {
                BeerRecipeID = input.BeerRecipeID,
                BoilingTimeInMinutes = input.BoilingTimeInMinutes,
                CustomAlphaAcids = input.CustomAlphaAcids,
                HopID = input.HopID,
                HoppingType = input.HoppingType,
                Quantity = input.Quantity
            };
            return await Repository.InsertAndGetIdAsync(hopUsed);
        }

        public async Task<HopUsedForRecipe> UpdateHops(HopsUsedForRecipeDto input)
        {
            var hopUsed = new HopUsedForRecipe()
            {
                BeerRecipeID = input.BeerRecipeID,
                BoilingTimeInMinutes = input.BoilingTimeInMinutes,
                CustomAlphaAcids = input.CustomAlphaAcids,
                HopID = input.HopID,
                HoppingType = input.HoppingType,
                Quantity = input.Quantity,
                Id = input.Id
            };
            return await Repository.InsertOrUpdateAsync(hopUsed);
        }

        public async Task<List<HopsUsedForRecipeDto>> GetRecipeHops(long id)
        {
            var result = await Repository.GetAllIncluding(x => x.Hop).Where(x => x.BeerRecipeID == id).ToListAsync(); 
            return ObjectMapper.Map<List<HopsUsedForRecipeDto>>(result);

        }

        public async Task<List<HopsUsedForRecipeDto>> GetHopsUsedForBoil(long id)
        {
            var result = await Repository.GetAllIncluding(x => x.Hop).Where(x => x.BeerRecipeID == id && (x.HoppingType == HoppingTypes.HoppingType.Gotowanie || x.HoppingType == HoppingTypes.HoppingType.Aromat)).ToListAsync();
            return ObjectMapper.Map<List<HopsUsedForRecipeDto>>(result);
        }

        public async Task<List<HopsUsedForRecipeDto>> GetHopsUsedForFermentation(long id)
        {
            var result = await Repository.GetAllIncluding(x => x.Hop).Where(x => x.BeerRecipeID == id && x.HoppingType == HoppingTypes.HoppingType.NaZimno).ToListAsync();
            return ObjectMapper.Map<List<HopsUsedForRecipeDto>>(result);
        }

    }
}
