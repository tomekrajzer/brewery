﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.HopsUsedForRecipe.Dto;
using Brewery.IngredientsUsedForRecipe.Dto;

namespace Brewery.HopsUsedForRecipe
{
    public interface IHopsUsedForRecipeAppService : IAsyncCrudAppService<HopsUsedForRecipeDto, long, PagedResultRequestDto, CreateHopsUsedForRecipeDto, HopsUsedForRecipeDto>
    {
        Task<long> Create2(HopsUsedForRecipeDto input);
        Task<List<HopsUsedForRecipeDto>> GetRecipeHops(long id);
    }
}
