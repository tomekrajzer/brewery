﻿using System;
using System.ComponentModel.DataAnnotations;
using Brewery.HoppingTypes;
using Brewery.Hops;
using Brewery.Hops.Dto;

namespace Brewery.HopsUsedForRecipe.Dto
{
    public class CreateHopsUsedForRecipeDto
    {
        public long? BeerRecipeID { get; set; }

        public int? HopID { get; set; }

        [Required]
        public HoppingType HoppingType { get; set; }

        [Required]
        public double Quantity { get; set; }

        [Required]
        public int BoilingTimeInMinutes { get; set; }

        public Hop Hop { get; set; }

        public double CustomAlphaAcids { get; set; }
    }
}
