﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Brewery.BeerRecipes;
using Brewery.FermentablesIngredients.Dto;
using Brewery.HoppingTypes;
using Brewery.Hops;

namespace Brewery.HopsUsedForRecipe.Dto
{
    [AutoMapTo(typeof(HopUsedForRecipe))]
    public class HopsUsedForRecipeDto : EntityDto<long>
    {
        public BeerRecipe BeerRecipe { get; set; }
        public long? BeerRecipeID { get; set; }

        public Hop Hop { get; set; }
        public int? HopID { get; set; }

        [Required]
        public HoppingType HoppingType { get; set; }

        [Required]
        public double Quantity { get; set; }

        [Required]
        public int BoilingTimeInMinutes { get; set; }

        public double CustomAlphaAcids { get; set; }
    }
}
