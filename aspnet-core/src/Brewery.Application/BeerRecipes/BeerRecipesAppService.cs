﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.Dependency;
using System.IO;
using CsvHelper;
using System.Text;
using System;
using Brewery.BeerRecipes.Dto;
using Brewery.Authorization;
using Brewery.FermentablesIngredients;
using Brewery.HopsUsedForRecipe;
using Brewery.Hops;
using Brewery.MashProcess;
using Brewery.IngredientsUsedForRecipe;
using Brewery.IngredientsUsedForRecipe.Dto;
using Brewery.HopsUsedForRecipe.Dto;
using Brewery.MashProcess.Dto;

namespace Brewery.BeerRecipes
{
    [AbpAuthorize(PermissionNames.Pages_BeerRecipes)]
    public class BeerRecipeAppService : AsyncCrudAppService<BeerRecipe, BeerRecipeDto, long, PagedAndSortedResultRequestDto, BeerRecipeDto, BeerRecipeDto>, IBeerRecipeAppService
    {
        private readonly IngredientsUsedForRecipeAppService _ingredientUsedService;
        private readonly FermentablesIngredientsAppService _ingredientService;
        private readonly HopsUsedForRecipeAppService _hopsUsedService;
        private readonly HopsAppService _hopsService;
        private readonly MashAppService _mashService;
        private readonly IAbpSession _session;

        public BeerRecipeAppService(IngredientsUsedForRecipeAppService ingredientUsedService,
            FermentablesIngredientsAppService ingredientService,
            HopsUsedForRecipeAppService hopsUsedService,
            HopsAppService hopsService,
            MashAppService mashService,
            IAbpSession session,
            IRepository<BeerRecipe, long> repository)
            : base(repository)
        {
            _ingredientUsedService = ingredientUsedService;
            _ingredientService = ingredientService;
            _hopsUsedService = hopsUsedService;
            _hopsService = hopsService;
            _mashService = mashService;
            _session = session;
        }

        public async Task<BeerRecipe> CreateRecipe(CreateBeerRecipeDto recipe)
        {

            var newRecipe = new BeerRecipe()
            {
                ABV = recipe.Recipe.ABV,
                BeerStyleID = recipe.Recipe.BeerStyleID,
                BitternessBalance = recipe.Recipe.BitternessBalance,
                BLG = recipe.Recipe.BLG,
                IsPrivate = recipe.Recipe.IsPrivate,
                Name = recipe.Recipe.Name,
                Notes = recipe.Recipe.Notes,
                SRM = recipe.Recipe.SRM,
                UserId = _session.UserId,
                BeerSize = recipe.Recipe.BeerSize,
                MashEfficiency = recipe.Recipe.MashEfficiency,
                WaterGrainRatio = recipe.Recipe.WaterGrainRatio,
                BoilTimeInMin = recipe.Recipe.BoilTimeInMin,
                LossesFromBoiling = recipe.Recipe.LossesFromBoiling,
                ColdHopingLosses = recipe.Recipe.ColdHopingLosses,
                FermentationLosses = recipe.Recipe.FermentationLosses,
                EvaporationRate = recipe.Recipe.EvaporationRate,
                BoilWortSize = recipe.Recipe.BoilWortSize
            };
            var result = await Repository.InsertAndGetIdAsync(newRecipe);
            foreach (var ingredient in recipe.Ingredients)
            {
                ingredient.BeerRecipeID = result;
                await _ingredientUsedService.Create2(ingredient);
            }
            foreach (var hop in recipe.Hops)
            {
                hop.BeerRecipeID = result;
                await _hopsUsedService.Create2(hop);
            }

            //TODO create ingredient if no ingredient.id
            return await Repository.GetAsync(result);
        }

        public async Task<BeerRecipe> UpdateRecipe(CreateBeerRecipeDto recipe)
        {

            var newRecipe = new BeerRecipe()
            {
                ABV = recipe.Recipe.ABV,
                BeerStyleID = recipe.Recipe.BeerStyleID,
                BitternessBalance = recipe.Recipe.BitternessBalance,
                BLG = recipe.Recipe.BLG,
                IsPrivate = recipe.Recipe.IsPrivate,
                Name = recipe.Recipe.Name,
                Notes = recipe.Recipe.Notes,
                SRM = recipe.Recipe.SRM,
                UserId = _session.UserId,
                Id = recipe.Recipe.Id,
                MashEfficiency = recipe.Recipe.MashEfficiency,
                BoilTimeInMin = recipe.Recipe.BoilTimeInMin,
                LossesFromBoiling = recipe.Recipe.LossesFromBoiling,
                ColdHopingLosses = recipe.Recipe.ColdHopingLosses,
                FermentationLosses = recipe.Recipe.FermentationLosses,
                WaterGrainRatio = recipe.Recipe.WaterGrainRatio,
                EvaporationRate = recipe.Recipe.EvaporationRate,
                BeerSize = recipe.Recipe.BeerSize,
                BoilWortSize = recipe.Recipe.BoilWortSize
            };
            await Repository.UpdateAsync(newRecipe);
            foreach (var ingredient in recipe.Ingredients)
            {
                ingredient.BeerRecipeID = recipe.Recipe.Id;
                await _ingredientUsedService.UpdateIngredient(ingredient);
            }
            foreach (var hop in recipe.Hops)
            {
                hop.BeerRecipeID = recipe.Recipe.Id;
                await _hopsUsedService.UpdateHops(ObjectMapper.Map<HopsUsedForRecipeDto>(hop));
            }

            //TODO create ingredient if no ingredient.id
            return await Repository.GetAsync(recipe.Recipe.Id);
        }
        public override async Task<PagedResultDto<BeerRecipeDto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            var userId = _session.UserId;
            var result = await Repository.GetAllIncluding(x => x.BeerStyle).Where(x => x.UserId == userId).OrderByDescending(x => x.CreationTime).ToListAsync();
            return new PagedResultDto<BeerRecipeDto>(result.Count(), ObjectMapper.Map<IReadOnlyList<BeerRecipeDto>>(result));
        }

        public async Task<BeerRecipeDto> GetRecipeById(long id)
        {
            var result = await Repository.GetAllIncluding(x => x.BeerStyle).FirstOrDefaultAsync(x => x.Id == id);
            return ObjectMapper.Map<BeerRecipeDto>(result);
        }
    }
}