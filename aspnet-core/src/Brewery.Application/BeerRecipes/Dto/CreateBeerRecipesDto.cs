using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Brewery.Authorization.Users;
using Brewery.BeerStyles;
using Brewery.HopsUsedForRecipe.Dto;
using Brewery.IngredientsUsedForRecipe.Dto;
using Brewery.MashProcess.Dto;

namespace Brewery.BeerRecipes.Dto
{
    public class CreateBeerRecipeDto
    {
        public BeerRecipeDto Recipe { get; set; }

        public List<HopsUsedForRecipeDto> Hops { get; set; }

        public List<IngredientUsedForRecipeDto> Ingredients { get; set; }
    }
}
