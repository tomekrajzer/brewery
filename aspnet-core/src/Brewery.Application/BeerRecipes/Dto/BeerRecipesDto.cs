using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Brewery.Authorization.Users;
using Brewery.BeerStyles;
using Brewery.MashProcess;

namespace Brewery.BeerRecipes.Dto
{
    [AutoMapTo(typeof(BeerRecipe))]
    public class BeerRecipeDto : EntityDto<long>
    {
        public string Name { get; set; }

        public string Notes { get; set; }

        public bool IsPrivate { get; set; }

        public double BLG { get; set; }

        public double ABV { get; set; }

        public double SRM { get; set; }

        public double BitternessBalance { get; set; }

        public int? WaterGrainRatio { get; set; }

        public double? MashEfficiency { get; set; }

        public double? BoilTimeInMin { get; set; }

        public double? EvaporationRate { get; set; }

        public double? LossesFromBoiling { get; set; }

        public double? FermentationLosses { get; set; }

        public double? ColdHopingLosses { get; set; }

        public int? BeerStyleID { get; set; }
        public BeerStyleParams BeerStyle { get; set; }

        public double BeerSize { get; set; }

        public double? BoilWortSize { get; set; }

    }
}
