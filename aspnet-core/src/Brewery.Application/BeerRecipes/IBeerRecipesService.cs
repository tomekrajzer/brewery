using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.BeerRecipes.Dto;

namespace Brewery.BeerRecipes
{
    public interface IBeerRecipeAppService : IAsyncCrudAppService<BeerRecipeDto, long, PagedAndSortedResultRequestDto, BeerRecipeDto, BeerRecipeDto>
    {

    }
}
