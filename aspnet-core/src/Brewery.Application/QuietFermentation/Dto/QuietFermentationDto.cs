﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.BeerRecipes.Dto;
using Brewery.Beers;
using Brewery.IngredientTypes;

namespace Brewery.QuietFermentationProcess.Dto
{
    [AutoMapTo(typeof(QuietFermentation))]
    public class QuietFermentationDto : EntityDto<long>
    {
        public Beer Beer { get; set; }
        public long? BeerID { get; set; }

        public DateTime? Date { get; set; }

        public DateTime? HopsAddDate { get; set; }

        public double? BLG { get; set; }

        public string Note { get; set; }
    }
}
