﻿using System;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Brewery.BoilProcess.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.FermentationProcess.Dto;
using Brewery.Hops.Dto;
using Brewery.MashProcess.Dto;
using Brewery.QuietFermentationProcess.Dto;

namespace Brewery.QuietFermentationProcess
{
    public interface IQuietFermentationAppService : IAsyncCrudAppService<QuietFermentationDto, long, PagedResultRequestDto, QuietFermentationDto, QuietFermentationDto>
    {

    }
}
