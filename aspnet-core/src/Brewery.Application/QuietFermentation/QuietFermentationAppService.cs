﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Brewery.Authorization;
using Brewery.BoilProcess.Dto;
using Brewery.FermentablesIngredients.Dto;
using Brewery.FermentationProcess.Dto;
using Brewery.Hops.Dto;
using Brewery.IngredientTypes;
using Brewery.MashProcess.Dto;
using Brewery.QuietFermentationProcess.Dto;

namespace Brewery.QuietFermentationProcess
{
    [AbpAuthorize(PermissionNames.Pages_BeerRecipes)]
    public class QuietFermentationAppService : AsyncCrudAppService<QuietFermentation, QuietFermentationDto, long, PagedResultRequestDto, QuietFermentationDto, QuietFermentationDto>, IQuietFermentationAppService
    {

        public QuietFermentationAppService(
            IRepository<QuietFermentation, long> repository)
            : base(repository)
        {

        }

        public async Task<QuietFermentationDto> GetByBeer(long id)
        {
            var result = await Repository.FirstOrDefaultAsync(x => x.BeerID == id);
            return ObjectMapper.Map<QuietFermentationDto>(result);
        }

        public async Task InsertOrUpdate(QuietFermentationDto fermentation)
        {
            var model = new QuietFermentation()
            {
                BeerID = fermentation.BeerID,
                Date = fermentation.Date,
                BLG = fermentation.BLG,
                Note = fermentation.Note,
                HopsAddDate = fermentation.HopsAddDate,
                Id = fermentation.Id
            };
            await Repository.InsertOrUpdateAsync(model);
        }

    }
}
