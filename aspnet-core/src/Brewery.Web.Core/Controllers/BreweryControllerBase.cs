using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace Brewery.Controllers
{
    public abstract class BreweryControllerBase: AbpController
    {
        protected BreweryControllerBase()
        {
            LocalizationSourceName = BreweryConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
