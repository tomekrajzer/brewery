using Microsoft.AspNetCore.Antiforgery;
using Brewery.Controllers;

namespace Brewery.Web.Host.Controllers
{
    public class AntiForgeryController : BreweryControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
