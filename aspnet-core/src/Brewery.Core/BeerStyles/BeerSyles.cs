﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;

namespace Brewery.BeerStyles
{
    [Table("AppBeerStyles")]
    public class BeerStyleParams : Entity<int>, IHasCreationTime
    {
        [Required]
        public double OGmin { get; set; }

        [Required]
        public double OGmax { get; set; }

        [Required]
        public double FGmin { get; set; }

        [Required]
        public double FGmax { get; set; }

        [Required]
        public double ABVmin { get; set; }

        [Required]
        public double ABVmax { get; set; }

        [Required]
        public double IBUmin { get; set; }

        [Required]
        public double IBUmax { get; set; }

        [Required]
        public double SRMmin { get; set; }

        [Required]
        public double SRMmax { get; set; }

        [Required]
        public string Name { get; set; }

        public DateTime CreationTime { get; set; }


        public BeerStyleParams()
        {
            CreationTime = Clock.Now;
        }
    }
}
