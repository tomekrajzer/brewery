﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.Beers;

namespace Brewery.BoilProcess
{
    [Table("AppBoil")]
    public class Boil : Entity<long>
    {
        [ForeignKey(nameof(BeerID))]
        public Beer Beer { get; set; }
        [Required]
        public long? BeerID { get; set; }

        public DateTime? Date { get; set; }

        public double? BLG { get; set; }

        public string Note { get; set; }

        public double? BoilWortSize { get; set; }

        public double? EvaporationRate { get; set; }
    }
}
