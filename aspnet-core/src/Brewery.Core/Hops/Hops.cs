﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace Brewery.Hops
{
    [Table("AppHop")]
    public class Hop : Entity<int>
    {
        [Required]
        public double AlphaAcids { get; set; }

        [Required]
        public string Name { get; set; }
        
    }
}
