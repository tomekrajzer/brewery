﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace Brewery.Yeasts
{
    [Table("AppYeast")]
    public class Yeast : Entity<int>
    {
        [Required]
        public string Name { get; set; }

    }
}
