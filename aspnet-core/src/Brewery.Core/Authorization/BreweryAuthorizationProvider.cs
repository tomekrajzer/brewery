﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace Brewery.Authorization
{
    public class BreweryAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_BeerStyles, L("BeerStyles"));
            context.CreatePermission(PermissionNames.Pages_Ingredients, L("Ingredients"));
            context.CreatePermission(PermissionNames.Pages_BeerRecipes, L("BeerRecipes"));
            context.CreatePermission(PermissionNames.Pages_Beers, L("Beers"));
            context.CreatePermission(PermissionNames.Pages_Hops, L("Hops"));
            context.CreatePermission(PermissionNames.Pages_Warehouse, L("Warehouse"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, BreweryConsts.LocalizationSourceName);
        }
    }
}
