﻿namespace Brewery.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";

        public const string Pages_Roles = "Pages.Roles";

        public const string Pages_BeerStyles = "Pages.BeerStyles";

        public const string Pages_Ingredients = "Pages.Ingredients";

        public const string Pages_BeerRecipes = "Pages.BeerRecipes";

        public const string Pages_Hops = "Pages.Hops";

        public const string Pages_Beers = "Pages.Beers";

        public const string Pages_Warehouse = "Pages.Warehouse";

    }
}
