﻿using Abp.Authorization;
using Brewery.Authorization.Roles;
using Brewery.Authorization.Users;

namespace Brewery.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
