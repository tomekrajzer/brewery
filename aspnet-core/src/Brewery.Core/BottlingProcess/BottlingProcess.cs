﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.Beers;

namespace Brewery.BottlingProcess
{
    [Table("AppBottlingProcess")]
    public class Bottling : Entity<long>
    {
        [ForeignKey(nameof(BeerID))]
        public Beer Beer { get; set; }
        [Required]
        public long? BeerID { get; set; }

        public DateTime? Date { get; set; }

        public double? BLG { get; set; }

        public double? Quantity { get; set; }

        public double? COLevel { get; set; }

        public double? GrainTemp { get; set; }
    }
}
