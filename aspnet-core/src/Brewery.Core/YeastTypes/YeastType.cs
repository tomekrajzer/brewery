﻿using System;
namespace Brewery.YeastTypes
{
    public enum YeastType
    {
        Ale = 1,
        Lager = 2,
        Pszeniczne = 3,
        Winiarskie = 4,
        Szampańskie = 5
    }
}
