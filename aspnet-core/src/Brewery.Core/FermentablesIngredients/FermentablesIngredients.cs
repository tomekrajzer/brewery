﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Brewery.IngredientTypes;

namespace Brewery.FermentablesIngredients
{
    [Table("AppFermentablesIngredient")]
    public class FermentablesIngredient : Entity<int>
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public double EBC { get; set; }

        [Required]
        public double Yield { get; set; }

        public string Desscription { get; set; }

        public string Supplier { get; set; }

        public double Moisture { get; set; }

        public double Protein { get; set; }

        [Required]
        public IngredientType IngredientType { get; set; }

    }
}
