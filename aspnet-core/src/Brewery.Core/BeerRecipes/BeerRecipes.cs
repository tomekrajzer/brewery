﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using Brewery.Authorization.Users;
using Brewery.BeerStyles;
using Brewery.MashProcess;

namespace Brewery.BeerRecipes
{
    [Table("AppBeerRecipe")]
    public class BeerRecipe : Entity<long>, IHasCreationTime
    {
        [Required]
        public string Name { get; set; }

        public string Notes { get; set; }

        [Required]
        public bool IsPrivate { get; set; }

        [Required]
        public double BLG { get; set; }

        [Required]
        public double ABV { get; set; }

        [Required]
        public double SRM { get; set; }

        [Required]
        public double BeerSize { get; set; }

        [Required]
        public double BitternessBalance { get; set; }

        [ForeignKey(nameof(BeerStyleID))]
        public BeerStyleParams BeerStyle { get; set; }
        public int? BeerStyleID { get; set; }

        [ForeignKey(nameof(UserId))]
        public User User { get; set; }
        public long? UserId { get; set; }

        public DateTime CreationTime { get; set; }

        [Required]
        public int? WaterGrainRatio { get; set; }

        [Required]
        public double? MashEfficiency { get; set; }

        public double? BoilTimeInMin { get; set; }

        public double? EvaporationRate { get; set; }

        public double? LossesFromBoiling { get; set; }

        public double? FermentationLosses { get; set; }

        public double? ColdHopingLosses { get; set; }

        public double? BoilWortSize { get; set; }

        public BeerRecipe()
        {
            CreationTime = Clock.Now;
        }
    }
}
