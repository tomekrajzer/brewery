﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.FermentablesIngredients;

namespace Brewery.IngredientsUsedForRecipe
{
    [Table("AppIngredientUsedForRecipe")]
    public class IngredientUsedForRecipe : Entity<long>
    {
        [ForeignKey(nameof(BeerRecipeID))]
        public BeerRecipe BeerRecipe { get; set; }
        [Required]
        public long? BeerRecipeID { get; set; }

        [Required]
        public double Quantity { get; set; }

        [Required]
        public double PercentageSize { get; set; }

        [ForeignKey(nameof(IngredientID))]
        public FermentablesIngredient Ingredient { get; set; }
        [Required]
        public int? IngredientID { get; set; }

        public double CustomYield { get; set; }

        public double CustomEBC { get; set; }

    }
}
