﻿using System;
namespace Brewery.IngredientTypes
{
    public enum IngredientType
    {
        Ziarno = 1,
        Cukier = 2,
        PłynnyEkstrakt = 3,
        SuchyEkstrakt = 4,
        Dodatek = 5
    }
}
