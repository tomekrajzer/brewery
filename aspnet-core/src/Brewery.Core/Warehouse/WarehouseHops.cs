﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Brewery.Authorization.Users;
using Brewery.FermentablesIngredients;
using Brewery.Hops;

namespace Brewery.WarehouseHops
{

    [Table("AppWarehouseHops")]
    public class WarehouseHop : Entity<int>
    {
        [Required]
        public double Quantity { get; set; }

        [Required]
        public double AlphaAcid { get; set; }

        [Required]
        public int Year { get; set; }

        [ForeignKey(nameof(HopID))]
        public Hop Hop { get; set; }
        public int? HopID { get; set; }

        [ForeignKey(nameof(UserId))]
        public User User { get; set; }
        public long? UserId { get; set; }
    }
}
