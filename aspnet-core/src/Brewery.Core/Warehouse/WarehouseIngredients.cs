﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Brewery.Authorization.Users;
using Brewery.FermentablesIngredients;
using Brewery.Hops;

namespace Brewery.WarehouseIngredients
{
    [Table("AppWarehouseIngredients")]
    public class WarehouseIngredient : Entity<int>
    {
        [Required]
        public double Quantity { get; set; }

        [Required]
        public double EBC { get; set; }

        [Required]
        public double Yield { get; set; }

        [ForeignKey(nameof(IngredientID))]
        public FermentablesIngredient Ingredient { get; set; }
        public int? IngredientID { get; set; }

        [ForeignKey(nameof(UserId))]
        public User User { get; set; }
        public long? UserId { get; set; }

    }
}
