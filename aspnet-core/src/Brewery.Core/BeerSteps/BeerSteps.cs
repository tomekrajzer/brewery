﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace Brewery.BeerSteps
{
    public enum BeerStep
    {
        Zacieranie = 1,
        Gotowanie = 2,
        FermentacjaBurzliwa = 3,
        FermentacjaCicha = 4,
        Rozlew = 5
    }
}
