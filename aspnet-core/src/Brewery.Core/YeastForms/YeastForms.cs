﻿using System;
namespace Brewery.YeastForms
{
    public enum YeastForm
    {
        Gestwa = 1,
        Płynne = 2,
        Suche = 3,
        Kultury = 4
    }
}
