﻿namespace Brewery
{
    public class BreweryConsts
    {
        public const string LocalizationSourceName = "Brewery";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = false;
    }
}
