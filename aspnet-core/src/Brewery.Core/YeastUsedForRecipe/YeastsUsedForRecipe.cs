﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.YeastForms;
using Brewery.Yeasts;
using Brewery.YeastTypes;

namespace Brewery.YeastsUsedForRecipe
{
    [Table("AppYeastUsedForRecipe")]
    public class YeastUsedForRecipe : Entity<long>
    {
        [Required]
        public double Quantity { get; set; }

        [ForeignKey(nameof(BeerRecipeID))]
        public BeerRecipe BeerRecipe { get; set; }
        [Required]
        public long? BeerRecipeID { get; set; }

        [ForeignKey(nameof(YeastID))]
        public Yeast Yeast { get; set; }
        public int? YeastID { get; set; }

        public YeastType YeastType { get; set; }

        public YeastForm YeastForm { get; set; }

    }
}
