﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using Brewery.Authorization.Users;
using Brewery.BeerRecipes;
using Brewery.BeerSteps;

namespace Brewery.Beers
{
    [Table("AppBeer")]
    public class Beer : Entity<long>, IHasCreationTime
    {
        [Required]
        public BeerStep ActualStep { get; set; }

        [ForeignKey(nameof(BeerRecipeID))]
        public BeerRecipe BeerRecipe { get; set; }
        public long? BeerRecipeID { get; set; }

        public int? BeerNumber { get; set; }

        public string Name { get; set; }
        
        public double? BLG { get; set; }

        public double? Size { get; set; }
        
        public DateTime CreationTime { get; set; }

        public DateTime? BottlingDate { get; set; }

        [ForeignKey(nameof(UserId))]
        public User User { get; set; }
        public long? UserId { get; set; }

        public Beer()
        {
            CreationTime = Clock.Now;
        }
    }
}
