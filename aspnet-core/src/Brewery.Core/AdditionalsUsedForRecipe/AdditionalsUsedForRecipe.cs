﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Brewery.AdditionalIngredientTypes;
using Brewery.BeerRecipes;
using Brewery.BeerSteps;

namespace Brewery.AdditionalsUsedForRecipe
{
    [Table("AppAdditionalUsedForRecipe")]
    public class AdditionalUsedForRecipe : Entity<long>
    {
        [Required]
        public int TimeInMin { get; set; }

        [Required]
        public double Quanity { get; set; }

        public BeerStep BeerStep { get; set; }

        [Required]
        public string Name { get; set; }

        [ForeignKey(nameof(BeerRecipeID))]
        public BeerRecipe BeerRecipe { get; set; }
        [Required]
        public long? BeerRecipeID { get; set; }

        public AdditionalIngredientType AdditionalIngredientType { get; set; }

    }
}
