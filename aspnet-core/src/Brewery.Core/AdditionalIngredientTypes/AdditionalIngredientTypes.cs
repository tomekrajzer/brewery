﻿using System;
namespace Brewery.AdditionalIngredientTypes
{
    public enum AdditionalIngredientType
    {
        Przyprawa = 1,
        Klarowanie = 2,
        CzynnikDoWody = 3,
        Zioło = 4,
        DodatekSmakowy = 5,
        Inne = 6
    }
}
