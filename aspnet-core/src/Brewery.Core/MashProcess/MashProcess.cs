﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.Beers;

namespace Brewery.MashProcess
{
    [Table("AppMash")]
    public class Mash : Entity<long>
    {
        [ForeignKey(nameof(BeerID))]
        public Beer Beer { get; set; }
        [Required]
        public long? BeerID { get; set; }

        public DateTime? Date { get; set; }

        public string Note { get; set; }

        public double? LauteringTemp { get; set; }

        public double? GrainTemp { get; set; }

        public double? WaterQuantityToMash { get; set; }
    }
}
