﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.Beers;

namespace Brewery.MashProcess
{
    [Table("AppMashSteps")]
    public class MashSteps : Entity<long>
    {

        [Required]
        public int TimeInMin { get; set; }

        [Required]
        public int Temperature { get; set; }

        [Required]
        public int StepOrder { get; set; }

        [ForeignKey(nameof(BeerID))]
        public Beer Beer { get; set; }
        [Required]
        public long? BeerID { get; set; }


    }
}
