﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.HoppingTypes;
using Brewery.Hops;

namespace Brewery.HopsUsedForRecipe
{
    [Table("AppHopsUsedForRecipe")]
    public class HopUsedForRecipe : Entity<long>
    {
        [ForeignKey(nameof(BeerRecipeID))]
        public BeerRecipe BeerRecipe { get; set; }
        [Required]
        public long? BeerRecipeID { get; set; }

        [ForeignKey(nameof(HopID))]
        public Hop Hop { get; set; }
        [Required]
        public int? HopID { get; set; }

        [Required]
        public HoppingType HoppingType { get; set; }

        [Required]
        public double Quantity { get; set; }

        [Required]
        public int BoilingTimeInMinutes { get; set; }

        public double CustomAlphaAcids { get; set; }

    }
}
