﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Brewery.BeerRecipes;
using Brewery.Beers;

namespace Brewery.FermentationProcess
{
    [Table("AppFermentation")]
    public class Fermentation : Entity<long>
    {
        [ForeignKey(nameof(BeerID))]
        public Beer Beer { get; set; }
        [Required]
        public long? BeerID { get; set; }

        public DateTime? Date { get; set; }

        public double? BLG { get; set; }

        public string Note { get; set; }

        public double? WortSize { get; set; }

        public double? BoilingLosses { get; set; }

    }
}
