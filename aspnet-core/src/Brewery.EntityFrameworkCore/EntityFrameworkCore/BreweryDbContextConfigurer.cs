using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Brewery.EntityFrameworkCore
{
    public static class BreweryDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<BreweryDbContext> builder, string connectionString)
        {
            builder.UseNpgsql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<BreweryDbContext> builder, DbConnection connection)
        {
            builder.UseNpgsql(connection);
        }
    }
}
