﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Brewery.Authorization.Roles;
using Brewery.Authorization.Users;
using Brewery.MultiTenancy;
using Brewery.BeerStyles;
using Brewery.Beers;
using Brewery.BeerRecipes;
using Brewery.AdditionalsUsedForRecipe;
using Brewery.FermentablesIngredients;
using Brewery.Hops;
using Brewery.HopsUsedForRecipe;
using Brewery.IngredientsUsedForRecipe;
using Brewery.MashProcess;
using Brewery.Yeasts;
using Brewery.YeastsUsedForRecipe;
using Brewery.BoilProcess;
using Brewery.FermentationProcess;
using Brewery.BottlingProcess;
using Brewery.QuietFermentationProcess;
using Brewery.WarehouseIngredients;
using Brewery.WarehouseHops;

namespace Brewery.EntityFrameworkCore
{
    public class BreweryDbContext : AbpZeroDbContext<Tenant, Role, User, BreweryDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public DbSet<BeerStyleParams> BeerStyles { get; set; }

        public DbSet<Beer> Beers { get; set; }

        public DbSet<BeerRecipe> BeerRecipes { get; set; }

        public DbSet<AdditionalUsedForRecipe> AdditionalsUsedForRecipe { get; set; }

        public DbSet<FermentablesIngredient> FermentablesIngredients { get; set; }

        public DbSet<Hop> Hops { get; set; }

        public DbSet<HopUsedForRecipe> HopsUsedForRecipe { get; set; }

        public DbSet<IngredientUsedForRecipe> IngredientsUsedForRecipe { get; set; }

        public DbSet<Mash> BeerMash { get; set; }

        public DbSet<Boil> BoilProcess { get; set; }

        public DbSet<Fermentation> FermentationProcess { get; set; }

        public DbSet<QuietFermentation> QuietFermentationProcess { get; set; }

        public DbSet<Bottling> BottlingProcess { get; set; }

        public DbSet<MashSteps> Mashsteps { get; set; }
        
        public DbSet<Yeast> Yeasts { get; set; }

        public DbSet<YeastUsedForRecipe> YeastsUsedForRecipe { get; set; }

        public DbSet<WarehouseHop> WarehouseHop { get; set; }

        public DbSet<WarehouseIngredient> WarehouseIngredients { get; set; }

        public BreweryDbContext(DbContextOptions<BreweryDbContext> options)
            : base(options)
        {
        }
    }
}
