﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Brewery.Configuration;
using Brewery.Web;

namespace Brewery.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class BreweryDbContextFactory : IDesignTimeDbContextFactory<BreweryDbContext>
    {
        public BreweryDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<BreweryDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            BreweryDbContextConfigurer.Configure(builder, configuration.GetConnectionString(BreweryConsts.ConnectionStringName));

            return new BreweryDbContext(builder.Options);
        }
    }
}
