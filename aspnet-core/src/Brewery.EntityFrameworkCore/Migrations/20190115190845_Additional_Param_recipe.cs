﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Additional_Param_recipe : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "BoilTimeInMin",
                table: "AppBeerRecipe",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "ColdHopingLosses",
                table: "AppBeerRecipe",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "EvaporationRate",
                table: "AppBeerRecipe",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "FermentationLosses",
                table: "AppBeerRecipe",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "LossesFromBoiling",
                table: "AppBeerRecipe",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BoilTimeInMin",
                table: "AppBeerRecipe");

            migrationBuilder.DropColumn(
                name: "ColdHopingLosses",
                table: "AppBeerRecipe");

            migrationBuilder.DropColumn(
                name: "EvaporationRate",
                table: "AppBeerRecipe");

            migrationBuilder.DropColumn(
                name: "FermentationLosses",
                table: "AppBeerRecipe");

            migrationBuilder.DropColumn(
                name: "LossesFromBoiling",
                table: "AppBeerRecipe");
        }
    }
}
