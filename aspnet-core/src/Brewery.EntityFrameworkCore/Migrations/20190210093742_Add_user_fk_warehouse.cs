﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Add_user_fk_warehouse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "AppWarehouseIngredients",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "AppWarehouseHops",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppWarehouseIngredients_UserId",
                table: "AppWarehouseIngredients",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AppWarehouseHops_UserId",
                table: "AppWarehouseHops",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppWarehouseHops_AbpUsers_UserId",
                table: "AppWarehouseHops",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppWarehouseIngredients_AbpUsers_UserId",
                table: "AppWarehouseIngredients",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppWarehouseHops_AbpUsers_UserId",
                table: "AppWarehouseHops");

            migrationBuilder.DropForeignKey(
                name: "FK_AppWarehouseIngredients_AbpUsers_UserId",
                table: "AppWarehouseIngredients");

            migrationBuilder.DropIndex(
                name: "IX_AppWarehouseIngredients_UserId",
                table: "AppWarehouseIngredients");

            migrationBuilder.DropIndex(
                name: "IX_AppWarehouseHops_UserId",
                table: "AppWarehouseHops");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "AppWarehouseIngredients");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "AppWarehouseHops");
        }
    }
}
