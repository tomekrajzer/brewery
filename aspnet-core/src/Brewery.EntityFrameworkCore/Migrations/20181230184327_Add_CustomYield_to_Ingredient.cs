﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Add_CustomYield_to_Ingredient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "CustomEBC",
                table: "AppIngredientUsedForRecipe",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "CustomYield",
                table: "AppIngredientUsedForRecipe",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomEBC",
                table: "AppIngredientUsedForRecipe");

            migrationBuilder.DropColumn(
                name: "CustomYield",
                table: "AppIngredientUsedForRecipe");
        }
    }
}
