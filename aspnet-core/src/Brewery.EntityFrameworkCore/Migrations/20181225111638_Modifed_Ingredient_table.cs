﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Modifed_Ingredient_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Extraction",
                table: "AppFermentablesIngredient",
                newName: "Yield");

            migrationBuilder.AddColumn<string>(
                name: "Desscription",
                table: "AppFermentablesIngredient",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Moisture",
                table: "AppFermentablesIngredient",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Protein",
                table: "AppFermentablesIngredient",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "Supplier",
                table: "AppFermentablesIngredient",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Desscription",
                table: "AppFermentablesIngredient");

            migrationBuilder.DropColumn(
                name: "Moisture",
                table: "AppFermentablesIngredient");

            migrationBuilder.DropColumn(
                name: "Protein",
                table: "AppFermentablesIngredient");

            migrationBuilder.DropColumn(
                name: "Supplier",
                table: "AppFermentablesIngredient");

            migrationBuilder.RenameColumn(
                name: "Yield",
                table: "AppFermentablesIngredient",
                newName: "Extraction");
        }
    }
}
