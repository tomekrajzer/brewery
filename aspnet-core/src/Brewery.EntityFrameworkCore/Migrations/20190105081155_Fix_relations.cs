﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Brewery.Migrations
{
    public partial class Fix_relations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppMash_AppBeerRecipe_BeerRecipeID",
                table: "AppMash");

            migrationBuilder.DropForeignKey(
                name: "FK_AppPlannedBeerSizeParam_AppBeerRecipe_BeerRecipeID",
                table: "AppPlannedBeerSizeParam");

            migrationBuilder.DropIndex(
                name: "IX_AppPlannedBeerSizeParam_BeerRecipeID",
                table: "AppPlannedBeerSizeParam");

            migrationBuilder.DropIndex(
                name: "IX_AppMash_BeerRecipeID",
                table: "AppMash");

            migrationBuilder.DropColumn(
                name: "BeerRecipeID",
                table: "AppPlannedBeerSizeParam");

            migrationBuilder.DropColumn(
                name: "BeerRecipeID",
                table: "AppMash");

            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "AppPlannedBeerSizeParam",
                nullable: false,
                oldClrType: typeof(int))
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<long>(
                name: "BeerParamID",
                table: "AppBeerRecipe",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "MashId",
                table: "AppBeerRecipe",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppBeerRecipe_BeerParamID",
                table: "AppBeerRecipe",
                column: "BeerParamID");

            migrationBuilder.CreateIndex(
                name: "IX_AppBeerRecipe_MashId",
                table: "AppBeerRecipe",
                column: "MashId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppBeerRecipe_AppPlannedBeerSizeParam_BeerParamID",
                table: "AppBeerRecipe",
                column: "BeerParamID",
                principalTable: "AppPlannedBeerSizeParam",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppBeerRecipe_AppMash_MashId",
                table: "AppBeerRecipe",
                column: "MashId",
                principalTable: "AppMash",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppBeerRecipe_AppPlannedBeerSizeParam_BeerParamID",
                table: "AppBeerRecipe");

            migrationBuilder.DropForeignKey(
                name: "FK_AppBeerRecipe_AppMash_MashId",
                table: "AppBeerRecipe");

            migrationBuilder.DropIndex(
                name: "IX_AppBeerRecipe_BeerParamID",
                table: "AppBeerRecipe");

            migrationBuilder.DropIndex(
                name: "IX_AppBeerRecipe_MashId",
                table: "AppBeerRecipe");

            migrationBuilder.DropColumn(
                name: "BeerParamID",
                table: "AppBeerRecipe");

            migrationBuilder.DropColumn(
                name: "MashId",
                table: "AppBeerRecipe");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "AppPlannedBeerSizeParam",
                nullable: false,
                oldClrType: typeof(long))
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<long>(
                name: "BeerRecipeID",
                table: "AppPlannedBeerSizeParam",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "BeerRecipeID",
                table: "AppMash",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppPlannedBeerSizeParam_BeerRecipeID",
                table: "AppPlannedBeerSizeParam",
                column: "BeerRecipeID");

            migrationBuilder.CreateIndex(
                name: "IX_AppMash_BeerRecipeID",
                table: "AppMash",
                column: "BeerRecipeID");

            migrationBuilder.AddForeignKey(
                name: "FK_AppMash_AppBeerRecipe_BeerRecipeID",
                table: "AppMash",
                column: "BeerRecipeID",
                principalTable: "AppBeerRecipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppPlannedBeerSizeParam_AppBeerRecipe_BeerRecipeID",
                table: "AppPlannedBeerSizeParam",
                column: "BeerRecipeID",
                principalTable: "AppBeerRecipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
