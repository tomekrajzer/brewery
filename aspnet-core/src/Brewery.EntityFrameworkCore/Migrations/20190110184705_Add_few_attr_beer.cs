﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Add_few_attr_beer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "BottledBeerTemp",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfBottling",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "SugarUsed",
                table: "AppBeer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BottledBeerTemp",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "DateOfBottling",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "SugarUsed",
                table: "AppBeer");
        }
    }
}
