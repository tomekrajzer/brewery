﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Brewery.Migrations
{
    public partial class Add_few_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppBeerRecipe",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    IsPrivate = table.Column<bool>(nullable: false),
                    BLG = table.Column<double>(nullable: false),
                    ABV = table.Column<double>(nullable: false),
                    SRM = table.Column<double>(nullable: false),
                    BitternessBalance = table.Column<double>(nullable: false),
                    BeerStyleID = table.Column<int>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppBeerRecipe", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppBeerRecipe_AppBeerStyles_BeerStyleID",
                        column: x => x.BeerStyleID,
                        principalTable: "AppBeerStyles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppFermentablesIngredient",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false),
                    Extraction = table.Column<double>(nullable: false),
                    EBC = table.Column<double>(nullable: false),
                    IngredientType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppFermentablesIngredient", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppHop",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AlphaAcids = table.Column<double>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppHop", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppYeast",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppYeast", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppAdditionalUsedForRecipe",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    TimeInMin = table.Column<int>(nullable: false),
                    Quanity = table.Column<double>(nullable: false),
                    BeerStep = table.Column<int>(nullable: false),
                    BeerRecipeID = table.Column<long>(nullable: true),
                    AdditionalIngredientType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppAdditionalUsedForRecipe", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppAdditionalUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                        column: x => x.BeerRecipeID,
                        principalTable: "AppBeerRecipe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppBeer",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ActualStep = table.Column<int>(nullable: false),
                    BeerRecipeID = table.Column<long>(nullable: true),
                    BeerNumber = table.Column<int>(nullable: false),
                    LauteringTemp = table.Column<double>(nullable: false),
                    GrainTemp = table.Column<double>(nullable: false),
                    BLGBeforeBoil = table.Column<double>(nullable: false),
                    BLGBeforeFermentation = table.Column<double>(nullable: false),
                    BLGBeforeSilentFermentation = table.Column<double>(nullable: false),
                    EndBLG = table.Column<double>(nullable: false),
                    MashQuantity = table.Column<double>(nullable: false),
                    BoilingLosses = table.Column<double>(nullable: false),
                    DateOfHopsAdded = table.Column<DateTime>(nullable: false),
                    BottledBeerQuantity = table.Column<double>(nullable: false),
                    CO2Level = table.Column<double>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppBeer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppBeer_AppBeerRecipe_BeerRecipeID",
                        column: x => x.BeerRecipeID,
                        principalTable: "AppBeerRecipe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppMash",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    WaterGrainRatio = table.Column<int>(nullable: false),
                    Efficiency = table.Column<double>(nullable: false),
                    TimeInMin = table.Column<int>(nullable: false),
                    Temperature = table.Column<int>(nullable: false),
                    BeerRecipeID = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppMash", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppMash_AppBeerRecipe_BeerRecipeID",
                        column: x => x.BeerRecipeID,
                        principalTable: "AppBeerRecipe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppPlannedBeerSizeParam",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    PlannedBeerSize = table.Column<double>(nullable: false),
                    BoilTimeInMin = table.Column<int>(nullable: false),
                    LossesFromBoiling = table.Column<double>(nullable: false),
                    EvaporationRate = table.Column<double>(nullable: false),
                    FermentationLosses = table.Column<double>(nullable: false),
                    ColdHopingLosses = table.Column<double>(nullable: false),
                    BoilWortSize = table.Column<double>(nullable: false),
                    WortBLGBeforeBoiling = table.Column<double>(nullable: false),
                    WortSizeAfterBoiling = table.Column<double>(nullable: false),
                    BeerSizeBeforeColdHoping = table.Column<double>(nullable: false),
                    BeerRecipeID = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppPlannedBeerSizeParam", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppPlannedBeerSizeParam_AppBeerRecipe_BeerRecipeID",
                        column: x => x.BeerRecipeID,
                        principalTable: "AppBeerRecipe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppIngredientUsedForRecipe",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    BeerRecipeID = table.Column<long>(nullable: true),
                    Quantity = table.Column<double>(nullable: false),
                    PercentageSize = table.Column<double>(nullable: false),
                    IngredientID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppIngredientUsedForRecipe", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppIngredientUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                        column: x => x.BeerRecipeID,
                        principalTable: "AppBeerRecipe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AppIngredientUsedForRecipe_AppFermentablesIngredient_Ingred~",
                        column: x => x.IngredientID,
                        principalTable: "AppFermentablesIngredient",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppHopsUsedForRecipe",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    BeerRecipeID = table.Column<long>(nullable: true),
                    HopID = table.Column<int>(nullable: true),
                    HoppingType = table.Column<int>(nullable: false),
                    Quantity = table.Column<double>(nullable: false),
                    BoilingTimeInMinutes = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppHopsUsedForRecipe", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppHopsUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                        column: x => x.BeerRecipeID,
                        principalTable: "AppBeerRecipe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AppHopsUsedForRecipe_AppHop_HopID",
                        column: x => x.HopID,
                        principalTable: "AppHop",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppYeastUsedForRecipe",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Quantity = table.Column<double>(nullable: false),
                    BeerRecipeID = table.Column<long>(nullable: true),
                    YeastID = table.Column<int>(nullable: true),
                    YeastType = table.Column<int>(nullable: false),
                    YeastForm = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppYeastUsedForRecipe", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppYeastUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                        column: x => x.BeerRecipeID,
                        principalTable: "AppBeerRecipe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AppYeastUsedForRecipe_AppYeast_YeastID",
                        column: x => x.YeastID,
                        principalTable: "AppYeast",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppBeerStepParam",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    BeerStep = table.Column<int>(nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    BeerID = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppBeerStepParam", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppBeerStepParam_AppBeer_BeerID",
                        column: x => x.BeerID,
                        principalTable: "AppBeer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppAdditionalUsedForRecipe_BeerRecipeID",
                table: "AppAdditionalUsedForRecipe",
                column: "BeerRecipeID");

            migrationBuilder.CreateIndex(
                name: "IX_AppBeer_BeerRecipeID",
                table: "AppBeer",
                column: "BeerRecipeID");

            migrationBuilder.CreateIndex(
                name: "IX_AppBeerRecipe_BeerStyleID",
                table: "AppBeerRecipe",
                column: "BeerStyleID");

            migrationBuilder.CreateIndex(
                name: "IX_AppBeerStepParam_BeerID",
                table: "AppBeerStepParam",
                column: "BeerID");

            migrationBuilder.CreateIndex(
                name: "IX_AppHopsUsedForRecipe_BeerRecipeID",
                table: "AppHopsUsedForRecipe",
                column: "BeerRecipeID");

            migrationBuilder.CreateIndex(
                name: "IX_AppHopsUsedForRecipe_HopID",
                table: "AppHopsUsedForRecipe",
                column: "HopID");

            migrationBuilder.CreateIndex(
                name: "IX_AppIngredientUsedForRecipe_BeerRecipeID",
                table: "AppIngredientUsedForRecipe",
                column: "BeerRecipeID");

            migrationBuilder.CreateIndex(
                name: "IX_AppIngredientUsedForRecipe_IngredientID",
                table: "AppIngredientUsedForRecipe",
                column: "IngredientID");

            migrationBuilder.CreateIndex(
                name: "IX_AppMash_BeerRecipeID",
                table: "AppMash",
                column: "BeerRecipeID");

            migrationBuilder.CreateIndex(
                name: "IX_AppPlannedBeerSizeParam_BeerRecipeID",
                table: "AppPlannedBeerSizeParam",
                column: "BeerRecipeID");

            migrationBuilder.CreateIndex(
                name: "IX_AppYeastUsedForRecipe_BeerRecipeID",
                table: "AppYeastUsedForRecipe",
                column: "BeerRecipeID");

            migrationBuilder.CreateIndex(
                name: "IX_AppYeastUsedForRecipe_YeastID",
                table: "AppYeastUsedForRecipe",
                column: "YeastID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppAdditionalUsedForRecipe");

            migrationBuilder.DropTable(
                name: "AppBeerStepParam");

            migrationBuilder.DropTable(
                name: "AppHopsUsedForRecipe");

            migrationBuilder.DropTable(
                name: "AppIngredientUsedForRecipe");

            migrationBuilder.DropTable(
                name: "AppMash");

            migrationBuilder.DropTable(
                name: "AppPlannedBeerSizeParam");

            migrationBuilder.DropTable(
                name: "AppYeastUsedForRecipe");

            migrationBuilder.DropTable(
                name: "AppBeer");

            migrationBuilder.DropTable(
                name: "AppHop");

            migrationBuilder.DropTable(
                name: "AppFermentablesIngredient");

            migrationBuilder.DropTable(
                name: "AppYeast");

            migrationBuilder.DropTable(
                name: "AppBeerRecipe");
        }
    }
}
