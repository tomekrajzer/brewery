﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Add_UserRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppHopsUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppHopsUsedForRecipe");

            migrationBuilder.DropForeignKey(
                name: "FK_AppHopsUsedForRecipe_AppHop_HopID",
                table: "AppHopsUsedForRecipe");

            migrationBuilder.AlterColumn<int>(
                name: "HopID",
                table: "AppHopsUsedForRecipe",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "BeerRecipeID",
                table: "AppHopsUsedForRecipe",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "AppBeerRecipe",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppBeerRecipe_UserId",
                table: "AppBeerRecipe",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AppBeer_UserId",
                table: "AppBeer",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppBeer_AbpUsers_UserId",
                table: "AppBeer",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppBeerRecipe_AbpUsers_UserId",
                table: "AppBeerRecipe",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppHopsUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppHopsUsedForRecipe",
                column: "BeerRecipeID",
                principalTable: "AppBeerRecipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AppHopsUsedForRecipe_AppHop_HopID",
                table: "AppHopsUsedForRecipe",
                column: "HopID",
                principalTable: "AppHop",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppBeer_AbpUsers_UserId",
                table: "AppBeer");

            migrationBuilder.DropForeignKey(
                name: "FK_AppBeerRecipe_AbpUsers_UserId",
                table: "AppBeerRecipe");

            migrationBuilder.DropForeignKey(
                name: "FK_AppHopsUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppHopsUsedForRecipe");

            migrationBuilder.DropForeignKey(
                name: "FK_AppHopsUsedForRecipe_AppHop_HopID",
                table: "AppHopsUsedForRecipe");

            migrationBuilder.DropIndex(
                name: "IX_AppBeerRecipe_UserId",
                table: "AppBeerRecipe");

            migrationBuilder.DropIndex(
                name: "IX_AppBeer_UserId",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "AppBeerRecipe");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "AppBeer");

            migrationBuilder.AlterColumn<int>(
                name: "HopID",
                table: "AppHopsUsedForRecipe",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<long>(
                name: "BeerRecipeID",
                table: "AppHopsUsedForRecipe",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddForeignKey(
                name: "FK_AppHopsUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppHopsUsedForRecipe",
                column: "BeerRecipeID",
                principalTable: "AppBeerRecipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppHopsUsedForRecipe_AppHop_HopID",
                table: "AppHopsUsedForRecipe",
                column: "HopID",
                principalTable: "AppHop",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
