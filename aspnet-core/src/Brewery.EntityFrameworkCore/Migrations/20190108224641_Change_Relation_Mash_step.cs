﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Change_Relation_Mash_step : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppMashSteps_AppBeerRecipe_BeerRecipeID",
                table: "AppMashSteps");

            migrationBuilder.RenameColumn(
                name: "BeerRecipeID",
                table: "AppMashSteps",
                newName: "BeerID");

            migrationBuilder.RenameIndex(
                name: "IX_AppMashSteps_BeerRecipeID",
                table: "AppMashSteps",
                newName: "IX_AppMashSteps_BeerID");

            migrationBuilder.AddForeignKey(
                name: "FK_AppMashSteps_AppBeer_BeerID",
                table: "AppMashSteps",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppMashSteps_AppBeer_BeerID",
                table: "AppMashSteps");

            migrationBuilder.RenameColumn(
                name: "BeerID",
                table: "AppMashSteps",
                newName: "BeerRecipeID");

            migrationBuilder.RenameIndex(
                name: "IX_AppMashSteps_BeerID",
                table: "AppMashSteps",
                newName: "IX_AppMashSteps_BeerRecipeID");

            migrationBuilder.AddForeignKey(
                name: "FK_AppMashSteps_AppBeerRecipe_BeerRecipeID",
                table: "AppMashSteps",
                column: "BeerRecipeID",
                principalTable: "AppBeerRecipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
