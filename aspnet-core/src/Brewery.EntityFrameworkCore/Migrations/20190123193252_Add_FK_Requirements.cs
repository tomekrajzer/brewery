﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Add_FK_Requirements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppAdditionalUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppAdditionalUsedForRecipe");

            migrationBuilder.DropForeignKey(
                name: "FK_AppBoil_AppBeer_BeerID",
                table: "AppBoil");

            migrationBuilder.DropForeignKey(
                name: "FK_AppBottlingProcess_AppBeer_BeerID",
                table: "AppBottlingProcess");

            migrationBuilder.DropForeignKey(
                name: "FK_AppFermentation_AppBeer_BeerID",
                table: "AppFermentation");

            migrationBuilder.DropForeignKey(
                name: "FK_AppMash_AppBeer_BeerID",
                table: "AppMash");

            migrationBuilder.DropForeignKey(
                name: "FK_AppMashSteps_AppBeer_BeerID",
                table: "AppMashSteps");

            migrationBuilder.DropForeignKey(
                name: "FK_AppQuietFermentation_AppBeer_BeerID",
                table: "AppQuietFermentation");

            migrationBuilder.DropForeignKey(
                name: "FK_AppYeastUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppYeastUsedForRecipe");

            migrationBuilder.AlterColumn<long>(
                name: "BeerRecipeID",
                table: "AppYeastUsedForRecipe",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "BeerID",
                table: "AppQuietFermentation",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "BeerID",
                table: "AppMashSteps",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "BeerID",
                table: "AppMash",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "BeerID",
                table: "AppFermentation",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "BeerID",
                table: "AppBottlingProcess",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "BeerID",
                table: "AppBoil",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "BeerRecipeID",
                table: "AppAdditionalUsedForRecipe",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AppAdditionalUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppAdditionalUsedForRecipe",
                column: "BeerRecipeID",
                principalTable: "AppBeerRecipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AppBoil_AppBeer_BeerID",
                table: "AppBoil",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AppBottlingProcess_AppBeer_BeerID",
                table: "AppBottlingProcess",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AppFermentation_AppBeer_BeerID",
                table: "AppFermentation",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AppMash_AppBeer_BeerID",
                table: "AppMash",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AppMashSteps_AppBeer_BeerID",
                table: "AppMashSteps",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AppQuietFermentation_AppBeer_BeerID",
                table: "AppQuietFermentation",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AppYeastUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppYeastUsedForRecipe",
                column: "BeerRecipeID",
                principalTable: "AppBeerRecipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppAdditionalUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppAdditionalUsedForRecipe");

            migrationBuilder.DropForeignKey(
                name: "FK_AppBoil_AppBeer_BeerID",
                table: "AppBoil");

            migrationBuilder.DropForeignKey(
                name: "FK_AppBottlingProcess_AppBeer_BeerID",
                table: "AppBottlingProcess");

            migrationBuilder.DropForeignKey(
                name: "FK_AppFermentation_AppBeer_BeerID",
                table: "AppFermentation");

            migrationBuilder.DropForeignKey(
                name: "FK_AppMash_AppBeer_BeerID",
                table: "AppMash");

            migrationBuilder.DropForeignKey(
                name: "FK_AppMashSteps_AppBeer_BeerID",
                table: "AppMashSteps");

            migrationBuilder.DropForeignKey(
                name: "FK_AppQuietFermentation_AppBeer_BeerID",
                table: "AppQuietFermentation");

            migrationBuilder.DropForeignKey(
                name: "FK_AppYeastUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppYeastUsedForRecipe");

            migrationBuilder.AlterColumn<long>(
                name: "BeerRecipeID",
                table: "AppYeastUsedForRecipe",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "BeerID",
                table: "AppQuietFermentation",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "BeerID",
                table: "AppMashSteps",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "BeerID",
                table: "AppMash",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "BeerID",
                table: "AppFermentation",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "BeerID",
                table: "AppBottlingProcess",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "BeerID",
                table: "AppBoil",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "BeerRecipeID",
                table: "AppAdditionalUsedForRecipe",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddForeignKey(
                name: "FK_AppAdditionalUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppAdditionalUsedForRecipe",
                column: "BeerRecipeID",
                principalTable: "AppBeerRecipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppBoil_AppBeer_BeerID",
                table: "AppBoil",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppBottlingProcess_AppBeer_BeerID",
                table: "AppBottlingProcess",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppFermentation_AppBeer_BeerID",
                table: "AppFermentation",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppMash_AppBeer_BeerID",
                table: "AppMash",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppMashSteps_AppBeer_BeerID",
                table: "AppMashSteps",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppQuietFermentation_AppBeer_BeerID",
                table: "AppQuietFermentation",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppYeastUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppYeastUsedForRecipe",
                column: "BeerRecipeID",
                principalTable: "AppBeerRecipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
