﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class SugarCalculator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "COLevel",
                table: "AppBottlingProcess",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "GrainTemp",
                table: "AppBottlingProcess",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "COLevel",
                table: "AppBottlingProcess");

            migrationBuilder.DropColumn(
                name: "GrainTemp",
                table: "AppBottlingProcess");
        }
    }
}
