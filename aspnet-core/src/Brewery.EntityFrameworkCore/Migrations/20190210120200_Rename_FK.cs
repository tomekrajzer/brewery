﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Rename_FK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppWarehouseIngredients_AppFermentablesIngredient_Ingrdient~",
                table: "AppWarehouseIngredients");

            migrationBuilder.RenameColumn(
                name: "IngrdientID",
                table: "AppWarehouseIngredients",
                newName: "IngredientID");

            migrationBuilder.RenameIndex(
                name: "IX_AppWarehouseIngredients_IngrdientID",
                table: "AppWarehouseIngredients",
                newName: "IX_AppWarehouseIngredients_IngredientID");

            migrationBuilder.AddForeignKey(
                name: "FK_AppWarehouseIngredients_AppFermentablesIngredient_Ingredien~",
                table: "AppWarehouseIngredients",
                column: "IngredientID",
                principalTable: "AppFermentablesIngredient",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppWarehouseIngredients_AppFermentablesIngredient_Ingredien~",
                table: "AppWarehouseIngredients");

            migrationBuilder.RenameColumn(
                name: "IngredientID",
                table: "AppWarehouseIngredients",
                newName: "IngrdientID");

            migrationBuilder.RenameIndex(
                name: "IX_AppWarehouseIngredients_IngredientID",
                table: "AppWarehouseIngredients",
                newName: "IX_AppWarehouseIngredients_IngrdientID");

            migrationBuilder.AddForeignKey(
                name: "FK_AppWarehouseIngredients_AppFermentablesIngredient_Ingrdient~",
                table: "AppWarehouseIngredients",
                column: "IngrdientID",
                principalTable: "AppFermentablesIngredient",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
