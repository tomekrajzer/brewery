﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Change_FK_MAshStep : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppMashSteps_AppMash_MashID",
                table: "AppMashSteps");

            migrationBuilder.RenameColumn(
                name: "MashID",
                table: "AppMashSteps",
                newName: "BeerID");

            migrationBuilder.RenameIndex(
                name: "IX_AppMashSteps_MashID",
                table: "AppMashSteps",
                newName: "IX_AppMashSteps_BeerID");

            migrationBuilder.AddForeignKey(
                name: "FK_AppMashSteps_AppBeer_BeerID",
                table: "AppMashSteps",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppMashSteps_AppBeer_BeerID",
                table: "AppMashSteps");

            migrationBuilder.RenameColumn(
                name: "BeerID",
                table: "AppMashSteps",
                newName: "MashID");

            migrationBuilder.RenameIndex(
                name: "IX_AppMashSteps_BeerID",
                table: "AppMashSteps",
                newName: "IX_AppMashSteps_MashID");

            migrationBuilder.AddForeignKey(
                name: "FK_AppMashSteps_AppMash_MashID",
                table: "AppMashSteps",
                column: "MashID",
                principalTable: "AppMash",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
