﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Add_CustomAcids_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppIngredientUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppIngredientUsedForRecipe");

            migrationBuilder.DropForeignKey(
                name: "FK_AppIngredientUsedForRecipe_AppFermentablesIngredient_Ingred~",
                table: "AppIngredientUsedForRecipe");

            migrationBuilder.AlterColumn<int>(
                name: "IngredientID",
                table: "AppIngredientUsedForRecipe",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "BeerRecipeID",
                table: "AppIngredientUsedForRecipe",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "AppAdditionalUsedForRecipe",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddForeignKey(
                name: "FK_AppIngredientUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppIngredientUsedForRecipe",
                column: "BeerRecipeID",
                principalTable: "AppBeerRecipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AppIngredientUsedForRecipe_AppFermentablesIngredient_Ingred~",
                table: "AppIngredientUsedForRecipe",
                column: "IngredientID",
                principalTable: "AppFermentablesIngredient",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppIngredientUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppIngredientUsedForRecipe");

            migrationBuilder.DropForeignKey(
                name: "FK_AppIngredientUsedForRecipe_AppFermentablesIngredient_Ingred~",
                table: "AppIngredientUsedForRecipe");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "AppAdditionalUsedForRecipe");

            migrationBuilder.AlterColumn<int>(
                name: "IngredientID",
                table: "AppIngredientUsedForRecipe",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<long>(
                name: "BeerRecipeID",
                table: "AppIngredientUsedForRecipe",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddForeignKey(
                name: "FK_AppIngredientUsedForRecipe_AppBeerRecipe_BeerRecipeID",
                table: "AppIngredientUsedForRecipe",
                column: "BeerRecipeID",
                principalTable: "AppBeerRecipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppIngredientUsedForRecipe_AppFermentablesIngredient_Ingred~",
                table: "AppIngredientUsedForRecipe",
                column: "IngredientID",
                principalTable: "AppFermentablesIngredient",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
