﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Boil_Fermentation_Column_fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BoilingLosses",
                table: "AppBoil");

            migrationBuilder.AddColumn<double>(
                name: "BoilingLosses",
                table: "AppFermentation",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BoilingLosses",
                table: "AppFermentation");

            migrationBuilder.AddColumn<double>(
                name: "BoilingLosses",
                table: "AppBoil",
                nullable: true);
        }
    }
}
