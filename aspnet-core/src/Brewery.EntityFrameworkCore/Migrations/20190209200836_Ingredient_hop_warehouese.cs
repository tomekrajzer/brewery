﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Brewery.Migrations
{
    public partial class Ingredient_hop_warehouese : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppWarehouseHops",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Quantity = table.Column<double>(nullable: false),
                    AlphaAcid = table.Column<double>(nullable: false),
                    Year = table.Column<int>(nullable: false),
                    HopID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppWarehouseHops", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppWarehouseHops_AppHop_HopID",
                        column: x => x.HopID,
                        principalTable: "AppHop",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppWarehouseIngredients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Quantity = table.Column<double>(nullable: false),
                    EBC = table.Column<double>(nullable: false),
                    Yield = table.Column<double>(nullable: false),
                    IngrdientID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppWarehouseIngredients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppWarehouseIngredients_AppFermentablesIngredient_Ingrdient~",
                        column: x => x.IngrdientID,
                        principalTable: "AppFermentablesIngredient",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppWarehouseHops_HopID",
                table: "AppWarehouseHops",
                column: "HopID");

            migrationBuilder.CreateIndex(
                name: "IX_AppWarehouseIngredients_IngrdientID",
                table: "AppWarehouseIngredients",
                column: "IngrdientID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppWarehouseHops");

            migrationBuilder.DropTable(
                name: "AppWarehouseIngredients");
        }
    }
}
