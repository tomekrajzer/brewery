﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Add_bottlingDate_toBeer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppBeerRecipe_AppMash_MashId",
                table: "AppBeerRecipe");

            migrationBuilder.DropIndex(
                name: "IX_AppBeerRecipe_MashId",
                table: "AppBeerRecipe");

            migrationBuilder.DropColumn(
                name: "MashId",
                table: "AppBeerRecipe");

            migrationBuilder.AddColumn<DateTime>(
                name: "BottlingDate",
                table: "AppBeer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BottlingDate",
                table: "AppBeer");

            migrationBuilder.AddColumn<long>(
                name: "MashId",
                table: "AppBeerRecipe",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppBeerRecipe_MashId",
                table: "AppBeerRecipe",
                column: "MashId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppBeerRecipe_AppMash_MashId",
                table: "AppBeerRecipe",
                column: "MashId",
                principalTable: "AppMash",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
