﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Brewery.Migrations
{
    public partial class Add_MashSteps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Temperature",
                table: "AppMash");

            migrationBuilder.DropColumn(
                name: "TimeInMin",
                table: "AppMash");

            migrationBuilder.CreateTable(
                name: "AppMashSteps",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    TimeInMin = table.Column<int>(nullable: false),
                    Temperature = table.Column<int>(nullable: false),
                    StepOrder = table.Column<int>(nullable: false),
                    BeerRecipeID = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppMashSteps", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppMashSteps_AppBeerRecipe_BeerRecipeID",
                        column: x => x.BeerRecipeID,
                        principalTable: "AppBeerRecipe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppMashSteps_BeerRecipeID",
                table: "AppMashSteps",
                column: "BeerRecipeID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppMashSteps");

            migrationBuilder.AddColumn<int>(
                name: "Temperature",
                table: "AppMash",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TimeInMin",
                table: "AppMash",
                nullable: false,
                defaultValue: 0);
        }
    }
}
