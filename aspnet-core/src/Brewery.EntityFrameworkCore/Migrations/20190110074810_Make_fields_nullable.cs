﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Brewery.Migrations
{
    public partial class Make_fields_nullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "WortSizeAfterBoiling",
                table: "AppPlannedBeerSizeParam",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "WortBLGBeforeBoiling",
                table: "AppPlannedBeerSizeParam",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "WaterQuantityToMash",
                table: "AppPlannedBeerSizeParam",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "LossesFromBoiling",
                table: "AppPlannedBeerSizeParam",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "FermentationLosses",
                table: "AppPlannedBeerSizeParam",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "EvaporationRate",
                table: "AppPlannedBeerSizeParam",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "ColdHopingLosses",
                table: "AppPlannedBeerSizeParam",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "BoilWortSize",
                table: "AppPlannedBeerSizeParam",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<int>(
                name: "BoilTimeInMin",
                table: "AppPlannedBeerSizeParam",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<double>(
                name: "BeerSizeBeforeColdHoping",
                table: "AppPlannedBeerSizeParam",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "MashQuantity",
                table: "AppBeer",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "LauteringTemp",
                table: "AppBeer",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "GrainTemp",
                table: "AppBeer",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "EndBLG",
                table: "AppBeer",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "CO2Level",
                table: "AppBeer",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "BottledBeerQuantity",
                table: "AppBeer",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "BoilingLosses",
                table: "AppBeer",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<int>(
                name: "BeerNumber",
                table: "AppBeer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<double>(
                name: "BLGBeforeSilentFermentation",
                table: "AppBeer",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "BLGBeforeFermentation",
                table: "AppBeer",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "BLGBeforeBoil",
                table: "AppBeer",
                nullable: true,
                oldClrType: typeof(double));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "WortSizeAfterBoiling",
                table: "AppPlannedBeerSizeParam",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "WortBLGBeforeBoiling",
                table: "AppPlannedBeerSizeParam",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "WaterQuantityToMash",
                table: "AppPlannedBeerSizeParam",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "LossesFromBoiling",
                table: "AppPlannedBeerSizeParam",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "FermentationLosses",
                table: "AppPlannedBeerSizeParam",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "EvaporationRate",
                table: "AppPlannedBeerSizeParam",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "ColdHopingLosses",
                table: "AppPlannedBeerSizeParam",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "BoilWortSize",
                table: "AppPlannedBeerSizeParam",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BoilTimeInMin",
                table: "AppPlannedBeerSizeParam",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "BeerSizeBeforeColdHoping",
                table: "AppPlannedBeerSizeParam",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "MashQuantity",
                table: "AppBeer",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "LauteringTemp",
                table: "AppBeer",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "GrainTemp",
                table: "AppBeer",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "EndBLG",
                table: "AppBeer",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "CO2Level",
                table: "AppBeer",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "BottledBeerQuantity",
                table: "AppBeer",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "BoilingLosses",
                table: "AppBeer",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BeerNumber",
                table: "AppBeer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "BLGBeforeSilentFermentation",
                table: "AppBeer",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "BLGBeforeFermentation",
                table: "AppBeer",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "BLGBeforeBoil",
                table: "AppBeer",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);
        }
    }
}
