﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Brewery.Migrations
{
    public partial class Reorgarnize_DB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppBeerRecipe_AppPlannedBeerSizeParam_BeerParamID",
                table: "AppBeerRecipe");

            migrationBuilder.DropForeignKey(
                name: "FK_AppMashSteps_AppBeer_BeerID",
                table: "AppMashSteps");

            migrationBuilder.DropTable(
                name: "AppBeerStepParam");

            migrationBuilder.DropTable(
                name: "AppPlannedBeerSizeParam");

            migrationBuilder.DropIndex(
                name: "IX_AppBeerRecipe_BeerParamID",
                table: "AppBeerRecipe");

            migrationBuilder.DropColumn(
                name: "Efficiency",
                table: "AppMash");

            migrationBuilder.DropColumn(
                name: "WaterGrainRatio",
                table: "AppMash");

            migrationBuilder.DropColumn(
                name: "BeerParamID",
                table: "AppBeerRecipe");

            migrationBuilder.DropColumn(
                name: "BLGBeforeBoil",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "BLGBeforeFermentation",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "BLGBeforeSilentFermentation",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "BoilingLosses",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "BottledBeerQuantity",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "BottledBeerTemp",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "CO2Level",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "DateOfBottling",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "DateOfHopsAdded",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "DateOfSilentFermentation",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "EndBLG",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "FermentationStartDate",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "GrainTemp",
                table: "AppBeer");

            migrationBuilder.DropColumn(
                name: "LauteringTemp",
                table: "AppBeer");

            migrationBuilder.RenameColumn(
                name: "BeerID",
                table: "AppMashSteps",
                newName: "MashID");

            migrationBuilder.RenameIndex(
                name: "IX_AppMashSteps_BeerID",
                table: "AppMashSteps",
                newName: "IX_AppMashSteps_MashID");

            migrationBuilder.RenameColumn(
                name: "SugarUsed",
                table: "AppBeer",
                newName: "Size");

            migrationBuilder.RenameColumn(
                name: "MashQuantity",
                table: "AppBeer",
                newName: "BLG");

            migrationBuilder.AddColumn<long>(
                name: "BeerID",
                table: "AppMash",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "AppMash",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "GrainTemp",
                table: "AppMash",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "LauteringTemp",
                table: "AppMash",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "AppMash",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "WaterQuantityToMash",
                table: "AppMash",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "BeerSize",
                table: "AppBeerRecipe",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "MashEfficiency",
                table: "AppBeerRecipe",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "WaterGrainRatio",
                table: "AppBeerRecipe",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "AppBoil",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    BeerID = table.Column<long>(nullable: true),
                    Date = table.Column<DateTime>(nullable: true),
                    BLG = table.Column<double>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    BoilWortSize = table.Column<double>(nullable: true),
                    BoilingLosses = table.Column<double>(nullable: true),
                    EvaporationRate = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppBoil", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppBoil_AppBeer_BeerID",
                        column: x => x.BeerID,
                        principalTable: "AppBeer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppBottlingProcess",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    BeerID = table.Column<long>(nullable: true),
                    Date = table.Column<DateTime>(nullable: true),
                    BLG = table.Column<double>(nullable: true),
                    Quantity = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppBottlingProcess", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppBottlingProcess_AppBeer_BeerID",
                        column: x => x.BeerID,
                        principalTable: "AppBeer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppFermentation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    BeerID = table.Column<long>(nullable: true),
                    Date = table.Column<DateTime>(nullable: true),
                    BLG = table.Column<double>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    WortSize = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppFermentation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppFermentation_AppBeer_BeerID",
                        column: x => x.BeerID,
                        principalTable: "AppBeer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppQuietFermentation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    BeerID = table.Column<long>(nullable: true),
                    Date = table.Column<DateTime>(nullable: true),
                    HopsAddDate = table.Column<DateTime>(nullable: true),
                    BLG = table.Column<double>(nullable: true),
                    Note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppQuietFermentation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppQuietFermentation_AppBeer_BeerID",
                        column: x => x.BeerID,
                        principalTable: "AppBeer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppMash_BeerID",
                table: "AppMash",
                column: "BeerID");

            migrationBuilder.CreateIndex(
                name: "IX_AppBoil_BeerID",
                table: "AppBoil",
                column: "BeerID");

            migrationBuilder.CreateIndex(
                name: "IX_AppBottlingProcess_BeerID",
                table: "AppBottlingProcess",
                column: "BeerID");

            migrationBuilder.CreateIndex(
                name: "IX_AppFermentation_BeerID",
                table: "AppFermentation",
                column: "BeerID");

            migrationBuilder.CreateIndex(
                name: "IX_AppQuietFermentation_BeerID",
                table: "AppQuietFermentation",
                column: "BeerID");

            migrationBuilder.AddForeignKey(
                name: "FK_AppMash_AppBeer_BeerID",
                table: "AppMash",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppMashSteps_AppMash_MashID",
                table: "AppMashSteps",
                column: "MashID",
                principalTable: "AppMash",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppMash_AppBeer_BeerID",
                table: "AppMash");

            migrationBuilder.DropForeignKey(
                name: "FK_AppMashSteps_AppMash_MashID",
                table: "AppMashSteps");

            migrationBuilder.DropTable(
                name: "AppBoil");

            migrationBuilder.DropTable(
                name: "AppBottlingProcess");

            migrationBuilder.DropTable(
                name: "AppFermentation");

            migrationBuilder.DropTable(
                name: "AppQuietFermentation");

            migrationBuilder.DropIndex(
                name: "IX_AppMash_BeerID",
                table: "AppMash");

            migrationBuilder.DropColumn(
                name: "BeerID",
                table: "AppMash");

            migrationBuilder.DropColumn(
                name: "Date",
                table: "AppMash");

            migrationBuilder.DropColumn(
                name: "GrainTemp",
                table: "AppMash");

            migrationBuilder.DropColumn(
                name: "LauteringTemp",
                table: "AppMash");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "AppMash");

            migrationBuilder.DropColumn(
                name: "WaterQuantityToMash",
                table: "AppMash");

            migrationBuilder.DropColumn(
                name: "BeerSize",
                table: "AppBeerRecipe");

            migrationBuilder.DropColumn(
                name: "MashEfficiency",
                table: "AppBeerRecipe");

            migrationBuilder.DropColumn(
                name: "WaterGrainRatio",
                table: "AppBeerRecipe");

            migrationBuilder.RenameColumn(
                name: "MashID",
                table: "AppMashSteps",
                newName: "BeerID");

            migrationBuilder.RenameIndex(
                name: "IX_AppMashSteps_MashID",
                table: "AppMashSteps",
                newName: "IX_AppMashSteps_BeerID");

            migrationBuilder.RenameColumn(
                name: "Size",
                table: "AppBeer",
                newName: "SugarUsed");

            migrationBuilder.RenameColumn(
                name: "BLG",
                table: "AppBeer",
                newName: "MashQuantity");

            migrationBuilder.AddColumn<double>(
                name: "Efficiency",
                table: "AppMash",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "WaterGrainRatio",
                table: "AppMash",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "BeerParamID",
                table: "AppBeerRecipe",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "BLGBeforeBoil",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "BLGBeforeFermentation",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "BLGBeforeSilentFermentation",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "BoilingLosses",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "BottledBeerQuantity",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "BottledBeerTemp",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "CO2Level",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfBottling",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfHopsAdded",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfSilentFermentation",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "EndBLG",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FermentationStartDate",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "GrainTemp",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "LauteringTemp",
                table: "AppBeer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AppBeerStepParam",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    BeerID = table.Column<long>(nullable: true),
                    BeerStep = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Notes = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppBeerStepParam", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppBeerStepParam_AppBeer_BeerID",
                        column: x => x.BeerID,
                        principalTable: "AppBeer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppPlannedBeerSizeParam",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    BeerSizeBeforeColdHoping = table.Column<double>(nullable: true),
                    BoilTimeInMin = table.Column<int>(nullable: true),
                    BoilWortSize = table.Column<double>(nullable: true),
                    ColdHopingLosses = table.Column<double>(nullable: true),
                    EvaporationRate = table.Column<double>(nullable: true),
                    FermentationLosses = table.Column<double>(nullable: true),
                    LossesFromBoiling = table.Column<double>(nullable: true),
                    PlannedBeerSize = table.Column<double>(nullable: false),
                    WaterQuantityToMash = table.Column<double>(nullable: true),
                    WortBLGBeforeBoiling = table.Column<double>(nullable: true),
                    WortSizeAfterBoiling = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppPlannedBeerSizeParam", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppBeerRecipe_BeerParamID",
                table: "AppBeerRecipe",
                column: "BeerParamID");

            migrationBuilder.CreateIndex(
                name: "IX_AppBeerStepParam_BeerID",
                table: "AppBeerStepParam",
                column: "BeerID");

            migrationBuilder.AddForeignKey(
                name: "FK_AppBeerRecipe_AppPlannedBeerSizeParam_BeerParamID",
                table: "AppBeerRecipe",
                column: "BeerParamID",
                principalTable: "AppPlannedBeerSizeParam",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppMashSteps_AppBeer_BeerID",
                table: "AppMashSteps",
                column: "BeerID",
                principalTable: "AppBeer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
